/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
/**
 *
 * @author eduardo.barbosa
 */
public class ClienteDAO {
    
     EntityManager em = PersistenceUtils.getEm();
     
    public Cliente getClienteById(final long id) {
        return em.find(Cliente.class, id);
     }
         
    public void insereDezClientes() {
         List<Animal> animais= new ArrayList<Animal>();
         em.getTransaction().begin();
         for(long j=100; j< 110; j++){
         Cliente c = new Cliente();
         c.setNome("Clotilde" + j);
          c.setSexo(SexoType.M);
          c.setProfissao("Laçador de Clóvis");
         for(long i =100; i<110; i++){
           Animal a = new Animal(j, "Clóvis " + i, SexoType.F);
           a.setIdCliente(c);
           em.persist(a);
         }
         }
         em.getTransaction().commit();
    }
}
