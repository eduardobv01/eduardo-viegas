/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
/**
 *
 * @author eduardo.barbosa
 */
public class AnimalDAO {
    
     EntityManager em = PersistenceUtils.getEm();
     
        public List<Animal> buscarClientePorNome(String nome) {
        Session session = null;
        try {
            session=HibernateUtil.getSessionFactory().openSession();
            return session.createCriteria(Cliente.class)
                    .add(Restrictions.ilike("nome", nome))
                    .list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
        
    public void insereCincoAnimais() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Animal animal = new Animal();
        animal.setNome("Teste");
        animal.setSexo(SexoType.F);
        animal.setValor(new BigDecimal(55.5));
        session.save(animal);

        session.getTransaction().commit();
         /*List<Animal> animais= new ArrayList<Animal>();
         em.getTransaction().begin();
         for(long i=0; i< 5; i++){
           Animal a = new Animal(null, "Clóvis " + i, SexoType.F);
           em.persist(a);
         }
         em.getTransaction().commit();*/
    }
        
        
        
        
   
}
