/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.localizacao.service;

import br.com.dbc.localizacao.entity.HibernateUtil;
import br.com.dbc.localizacao.entity.Pais;
import br.com.dbc.localizacao.entity.PersistenceUtils;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author tiago
 */
public class PaisService {

    private static PaisService instance;

    public static PaisService getInstance() {
        return instance == null ? new PaisService() : instance;
    }

    private PaisService() {
    }

    public List<Pais> buscarPaisPorNome(String nome) {
        return PersistenceUtils.getEm()
                .createQuery("select p from Pais p where p.nome like :nome")
                .setParameter("nome", nome)
                .getResultList();

    }

    public List<Pais> buscarPaisPorNomeCriteria(String nome) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createCriteria(Pais.class)
                    .add(Restrictions.ilike("nome", nome))
                    .list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}
