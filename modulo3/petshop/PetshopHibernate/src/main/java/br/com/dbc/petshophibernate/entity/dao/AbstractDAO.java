/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.entity.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author eduardo.barbosa
 */
public abstract class AbstractDAO<ENTITY, ID> {//Entity e ID são generics. Podem ser qualquer coisa.
    private Class<ENTITY> entityClass;//Uma classe que é do tipo entity que a generic está passando é um atrivuto de classe
    private String idProperty;
    
    public AbstractDAO(Class entityClass, String idProperty){
       this.entityClass = entityClass;
       this.idProperty = idProperty;
    }
    
    public ENTITY findById(Long id){
        return (ENTITY) HibernateUtil.getSessionFactory()
                .openSession()
                .createCriteria(entityClass)
                .add(Restrictions.eq(idProperty, id))
                .uniqueResult();
    }
    
    public void createOrUpdate(ENTITY e){
        Session session = null;
        try{
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(e);
        transaction.commit();
        session.close();
        }catch(Exception ex){
            ex.printStackTrace();
            throw ex;
        }finally{
            if(session != null)
            session.close();
        }
    }
    
}
/*
public ENTITY findById(Long id){
        return entityClass.
                cast(HibernateUtil.getSessionFactory()
                .openSession()
                .createCriteria(entityClass)
                .add(Restrictions.eq(idProperty, id))
                .uniqueResult());
    }
*/