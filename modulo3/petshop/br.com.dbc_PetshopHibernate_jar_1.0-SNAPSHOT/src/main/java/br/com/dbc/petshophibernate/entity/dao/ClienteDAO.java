/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.entity.dao;

import br.com.dbc.petshophibernate.entity.Cliente;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author eduardo.barbosa
 */
public class ClienteDAO {
    private static final ClienteDAO instance;
    static{
        instance = new ClienteDAO();
    }
    
    public Cliente findById(Long id){
        return (Cliente) HibernateUtil.getSessionFactory()
                .openSession()
                .createCriteria(Cliente.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }
}
