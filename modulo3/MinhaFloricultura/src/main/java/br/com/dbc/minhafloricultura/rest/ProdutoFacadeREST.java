/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.minhafloricultura.dao.ClienteDAO;
import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author eduardo.barbosa
 */
@Stateless//bean que não salva o estado do objeto
@Path("produto")
public class ProdutoFacadeREST extends AbstractCrudREST<Produto, ProdutoDAO> {

    @Inject//Depende dos metodos de DAO
    private ProdutoDAO produtoDAO;

    @Override
    protected ProdutoDAO getDAO() {
        return produtoDAO;
    }
    
}
