/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.ws;

import br.com.dbc.minhafloricultura.dao.AbstractDAO;
import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;

/**
 *
 * @author eduardo.barbosa
 */
@Stateless
@WebService(serviceName = "ProdutoWS")
public class ProdutoWS extends AbstractCrudWS<ProdutoDAO, Produto>{

    @EJB
    private ProdutoDAO produtoDAO;// Add business logic below. (Right-click in editor and choose
    // "Web Service > Add Operation"

    @Override
    @WebMethod(exclude=true)
    public ProdutoDAO getDAO() {
        return produtoDAO; //To change body of generated methods, choose Tools | Templates.
    }

    


    
}
