/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.dao;

import br.com.dbc.minhafloricultura.entity.Produto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author eduardo.barbosa
 */
@Stateless
public class ProdutoDAO extends AbstractDAO<Produto> {//Possui os métodos de Abstract direcionados para entidade Produto.

    @PersistenceContext(unitName = "minha_floricultura_jndi")//Torna desnecssário a criação da factory
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProdutoDAO() {
        super(Produto.class);
    }
    
}
