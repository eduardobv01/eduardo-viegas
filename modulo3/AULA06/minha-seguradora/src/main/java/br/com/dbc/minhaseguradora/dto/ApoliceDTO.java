/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhaseguradora.dto;

import lombok.Data;

/**
 *
 * @author tiago
 */
@Data
public class ApoliceDTO {
    private String nome;
    private String periodo;
}
