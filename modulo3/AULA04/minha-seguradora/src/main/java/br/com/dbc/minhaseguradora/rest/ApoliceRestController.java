package br.com.dbc.minhaseguradora.rest;

import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.service.ApoliceService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.data.domain.Pageable;


@RestController
@RequestMapping("/url")
public class ApoliceRestController {
    
    @Autowired
    private ApoliceService apoliceService;
    
    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok( apoliceService.buscarTodos( pageable ) );
    }
    
    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable Long id) {
        return apoliceService.buscarPorId(id)
                .map(ResponseEntity::ok)
                .orElse( ResponseEntity.notFound().build() );
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody Apolice apolice) {
        if(id == null || !Objects.equals(apolice.getId(), id))
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(apoliceService.salvar(apolice));
    }
    
    @PostMapping
    public ResponseEntity<?> post(@RequestBody Apolice input) {
        if (input.getId() != null)
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(apoliceService.salvar(input));
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        apoliceService.deletar(id);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error message")
    public void handleError() {
    }
    
}
