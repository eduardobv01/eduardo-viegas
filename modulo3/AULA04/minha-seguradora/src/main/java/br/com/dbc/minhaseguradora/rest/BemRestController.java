/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhaseguradora.rest;

import br.com.dbc.minhaseguradora.entity.Bem;
import br.com.dbc.minhaseguradora.service.BemService;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

/**
 *
 * @author tiago
 */
@RestController
@RequestMapping("/api/bem")
public class BemRestController {

    @Autowired
    private BemService bemService;

    @GetMapping()
    public ResponseEntity<Page<Bem>> listar(Pageable pageable) {
        return ResponseEntity.ok(bemService.listar(pageable));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editar(@PathVariable Long id, @Valid @RequestBody Bem input) {
        return ResponseEntity.ok(bemService.editar(id, input));
    }

    @PostMapping
    public ResponseEntity<?> criar(@RequestBody Bem input) {
        return ResponseEntity.ok(bemService.criar(input));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> remover(@PathVariable Long id) {
        bemService.remover(id);
        return ResponseEntity.ok().build();
    }


}
