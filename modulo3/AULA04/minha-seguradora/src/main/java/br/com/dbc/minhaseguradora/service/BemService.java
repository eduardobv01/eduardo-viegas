package br.com.dbc.minhaseguradora.service;

import br.com.dbc.minhaseguradora.entity.Bem;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.dbc.minhaseguradora.repository.BemRepository;
import org.springframework.beans.factory.annotation.Autowired;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class BemService {

    @Autowired
    private final BemRepository bemRepository;

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Bem criar(@NotNull @Valid Bem cliente) {
        return bemRepository.save(cliente);
    }

    public Page<Bem> listar(Pageable pageable) {
        return bemRepository.findAll(pageable);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void remover(Long id) {
        bemRepository.deleteById(id);
    }

    public Bem editar(Long id, Bem bem) {
        bem.setId(id);
        return bemRepository.save(bem);
    }

}
