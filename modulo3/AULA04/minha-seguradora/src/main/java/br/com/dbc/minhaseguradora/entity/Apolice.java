package br.com.dbc.minhaseguradora.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Apolice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descricao;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate inicioVigencia;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fimVigencia;
    
    
}