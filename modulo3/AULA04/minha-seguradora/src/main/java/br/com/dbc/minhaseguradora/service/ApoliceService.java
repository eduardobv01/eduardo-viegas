package br.com.dbc.minhaseguradora.service;

import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.repository.ApoliceRepository;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class ApoliceService {

    @Autowired
    private ApoliceRepository apoliceRepository;
    
    //save
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Apolice salvar( Apolice apolice ){
        return apoliceRepository.save(apolice);
    }
    //findById
    public Optional<Apolice> buscarPorId( Long id ){
        return apoliceRepository.findById(id);
    }    
    //findAll
    public Page<Apolice> buscarTodos( Pageable pa ){
        return apoliceRepository.findAll( pa );
    }
    //delete
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deletar( Long id ){
        apoliceRepository.deleteById(id);
    }
    
}
