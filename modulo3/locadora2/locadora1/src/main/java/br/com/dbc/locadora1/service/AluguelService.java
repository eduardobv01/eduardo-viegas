/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.dto.AluguelDTO;
import br.com.dbc.locadora1.repository.AluguelRepository;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class AluguelService extends AbstractService<Aluguel> {

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    public JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }

    
    public BigDecimal cacularMulta() {

        return new BigDecimal("15.5");
    }
    // private boolean isAlugada(){
    //return true;     
    //}

    public Aluguel alugar(AluguelDTO aluguelDTO) {
        Optional<Cliente> c = this.clienteService.findById(aluguelDTO.getIdCliente());
        if (c.isPresent()) {
            System.out.println(c.get().getNome());
            Aluguel aluguel = Aluguel.builder()
                    .retirada(LocalDate.now())
                    .previsao(LocalDate.now().plusDays(aluguelDTO.getIdMidias().size()))
                    .multa(new BigDecimal("0.0"))
                    .idCliente(c.get())
                    .build();

            this.aluguelRepository.save(aluguel);

            aluguelDTO.getIdMidias().forEach(idMidia -> {
                Optional<Midia> midia = midiaService.findById(idMidia);
                if (midia.isPresent()) {
                    midia.get().setIdAluguel(aluguel);
                    //Incluir valor do aluguel ValorBigDecimal.add(valorIncremental)  
                    midiaService.save(midia.get());
                }
            });
            // 
            return aluguel;
        }
        return null;
    }
    
    public Aluguel devolver(List<Long> midiasDevolvidasId) {
        Midia midiaDoAluguel = midiaService.findById(midiasDevolvidasId.get(0)).get();
        Aluguel aluguel = midiaDoAluguel.getIdAluguel();
        List<Midia> midias = midiaService.findByIdAluguel(aluguel);
        List<Midia> midiasDevolvidas = new ArrayList();
        for (Long idMidia : midiasDevolvidasId) {
            for (Midia midiaObj : midias) {
                if (midiaObj.getId() == idMidia) {//Se existe nas midias devolvidas
                    midiasDevolvidas.add(midiaObj);
                }
                Optional<Midia> midiaAtual = midiaService.findById(idMidia);
                midiaAtual.get().setIdAluguel(null);
                
                midiaService.save(midiaAtual.get());
                midias.removeAll(midiasDevolvidas);
                
            }
        }
        return this.aluguelRepository.save(aluguel);
        /*Midia midia;
        for (int i = 0; i < midiasDevolvidas.size(); i++) {
            midia = this.midiaService.findById(midiasDevolvidas.get(i)).orElse(null);
            midia.setIdAluguel(null);
            midiaService.save(midia);
            Aluguel aluguel = midia.getIdAluguel();
            aluguel.setDevolucao(LocalDate.now());
        }*/
    }

    /*public /*List<Aluguel>void getFilmesDevolverHoje(){
        List<Aluguel> alugueis = this.aluguelRepository.findByPrevisao(LocalDate.now());
        List<Filme> filmes = new ArrayList();
        final List<Midia> midias;
            midias = this.midiaService.findByIdAluguel(aluguel);
           
            
           filmes.add(e)
        });*/
    //}

}
