/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.repository;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hadaward
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    public int countByMidiaTypeAndIdFilme(MidiaType midiaType, Filme idFilme );
    
    public List<Midia> findByIdAluguel(Aluguel idAluguel);
    
    public List<Midia> findByIdFilme(Filme idFilme);
}
