/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author eduardo.barbosa
 */
@Entity
@Table(name = "MIDIA")
@NamedQueries({
    @NamedQuery(name = "Midia.findAll", query = "SELECT m FROM Midia m")
    , @NamedQuery(name = "Midia.findById", query = "SELECT m FROM Midia m WHERE m.id = :id")
    , @NamedQuery(name = "Midia.findByMidiaType", query = "SELECT m FROM Midia m WHERE m.midiaType = :midiaType")})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Midia extends AbstractEntity implements Serializable{
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//Utilizar no banco em memória
    @Column(name = "ID")
    private Long id;
    @Column(name = "MIDIA_TYPE")
    @Enumerated(value = EnumType.STRING)
    private MidiaType midiaType;
    @JoinColumn(name = "ID_ALUGUEL", referencedColumnName = "ID")
    @ManyToOne
    private Aluguel idAluguel;
    @JoinColumn(name = "ID_FILME", referencedColumnName = "ID")
    @ManyToOne
    private Filme idFilme;
}
