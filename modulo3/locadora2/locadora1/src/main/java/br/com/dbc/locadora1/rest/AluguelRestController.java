/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.dto.AluguelDTO;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author hadaward
 */
@RestController
@RequestMapping("/api/aluguel")
public class AluguelRestController extends AbstractRestController<Aluguel> {

    @Autowired
    private AluguelService aluguelService;

    @Override
    public AbstractService<Aluguel> getService() {//Generic recebe AlguelService, que extende AbstractService
        return this.aluguelService;
    }

    @PostMapping("/retirada")
    public ResponseEntity<?> retirada(@RequestBody AluguelDTO aluguelDTO) {
        System.out.println(aluguelDTO.getIdCliente());
        System.out.println(aluguelDTO.getIdMidias());
        if (aluguelDTO==null||aluguelDTO.getIdCliente() == null || aluguelDTO.getIdMidias().isEmpty())
            return ResponseEntity.badRequest().build();
        System.out.println(this.aluguelService.alugar(aluguelDTO).getId());
        return ResponseEntity.ok(this.aluguelService.alugar(aluguelDTO));
    }
}
