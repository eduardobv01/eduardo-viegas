/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity.dto;

import br.com.dbc.locadora1.entity.AbstractEntity;
import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.rest.ClienteRestController;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author hadaward
 */
@Data
public class ClienteDTO {
     
    private Long id;
    private String nome;
    private String endereco;
    private String telefone;
    
    public ClienteDTO(Cliente cliente){
         this.nome=cliente.getNome();
         this.endereco=cliente.getEndereco();
         this.telefone=cliente.getTelefone();
    }
   
  
}
