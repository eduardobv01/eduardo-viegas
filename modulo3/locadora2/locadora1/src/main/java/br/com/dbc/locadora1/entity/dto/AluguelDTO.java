/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity.dto;

import java.util.ArrayList;
import lombok.Data;

/**
 *
 * @author hadaward
 */
@Data
public class AluguelDTO {
    private Long idCliente;
    private ArrayList<Long> idMidias;
}
