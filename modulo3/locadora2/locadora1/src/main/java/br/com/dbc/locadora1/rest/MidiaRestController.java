/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.MidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author hadaward
 */
@RestController
@RequestMapping("/api/midia")
public class MidiaRestController extends AbstractRestController<Midia>{

    @Autowired
    private MidiaService midiaService;
    
    @Override
    public AbstractService<Midia> getService() {
        return this.midiaService;
    }
    
}
