/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.repository;

import br.com.dbc.locadora1.entity.ValorMidia;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hadaward
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {
    
}
