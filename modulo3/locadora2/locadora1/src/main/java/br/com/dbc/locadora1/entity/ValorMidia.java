/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author eduardo.barbosa
 */
@Entity
@Table(name = "VALOR_MIDIA")
@NamedQueries({
    @NamedQuery(name = "ValorMidia.findAll", query = "SELECT v FROM ValorMidia v")
    , @NamedQuery(name = "ValorMidia.findById", query = "SELECT v FROM ValorMidia v WHERE v.id = :id")
    , @NamedQuery(name = "ValorMidia.findByValor", query = "SELECT v FROM ValorMidia v WHERE v.valor = :valor")
    , @NamedQuery(name = "ValorMidia.findByInicioVigencia", query = "SELECT v FROM ValorMidia v WHERE v.inicioVigencia = :inicioVigencia")
    , @NamedQuery(name = "ValorMidia.findByFinalVigencia", query = "SELECT v FROM ValorMidia v WHERE v.finalVigencia = :finalVigencia")})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValorMidia implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//Utilizar no banco em memória
    @Column(name = "ID")
    private Long id;
    @NotNull
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Column(name = "INICIO_VIGENCIA")
    @DateTimeFormat(pattern = "DD/MM/YYYY")
    private LocalDateTime inicioVigencia;
    @Column(name = "FINAL_VIGENCIA")
    @DateTimeFormat(pattern = "DD/MM/YYYY")
    private LocalDateTime finalVigencia;
    @JoinColumn(name = "ID_MIDIA", referencedColumnName = "ID")
    @ManyToOne
    private Midia idMidia;
}
