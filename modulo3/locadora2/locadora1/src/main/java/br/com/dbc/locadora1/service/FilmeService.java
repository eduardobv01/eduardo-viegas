/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import br.com.dbc.locadora1.entity.dto.FilmeDTO;
import br.com.dbc.locadora1.repository.FilmeRepository;
import br.com.dbc.locadora1.repository.MidiaRepository;
import java.time.LocalDate;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FilmeService extends AbstractService<Filme> {

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private MidiaService midiaService;
    
    @Override
    public JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }

    public Page<Filme> buscar(String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoFim, Pageable pageable){ 
        return this.filmeRepository.findByTituloOrCategoriaOrLancamentoBetween(titulo, categoria, lancamentoIni, lancamentoFim, pageable); //
    }
               
    public int getQuantidadePorMidia(MidiaType midiaType, Filme filme) {
        return this.midiaRepository.countByMidiaTypeAndIdFilme(midiaType, filme);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme save(FilmeDTO filmeDTO) {
        Filme filme = this.filmeRepository.save(filmeDTO.toFilme());
        filmeDTO.getMidia().forEach(midia->{
            midiaService.save(midia, filme);
        });
        return filmeRepository.findById(filme.getId()).get();//[Temporário] Para testar requisição
    }
  
    /*
    public void exibirFilme(Long id) {
        System.out.println("br.com.dbc.locadora1.service.FilmeService.exibirFilme()");
        Filme filme = this.filmeRepository.findById(id).get();
        if(filme!=null){
        System.out.println(filme.getTitulo());
        System.out.println(filme.getLancamento());
        System.out.println(filme.getCategoria());
        }else{
            System.out.println("Filme nulo");
        }
    }*/
}
