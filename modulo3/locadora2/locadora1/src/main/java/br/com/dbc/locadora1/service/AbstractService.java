/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
public abstract class AbstractService<E>{
    
    @Autowired
    public abstract JpaRepository<E, Long> getRepository();
    
     @Transactional(readOnly = false, rollbackFor = Exception.class)
    public E save(E entity){
        return this.getRepository().save(entity);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete( Long id ){
        this.getRepository().deleteById(id);
    }
    
    public Optional<E> findById( Long id ){
        return this.getRepository().findById(id);
    }

    public Page<E> findAll( Pageable pa ){
        return this.getRepository().findAll( pa );
    }
}
