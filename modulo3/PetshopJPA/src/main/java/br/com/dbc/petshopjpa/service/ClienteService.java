/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.ClienteDAO;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.List;

/**
 *
 * @author eduardo.barbosa
 */
public class ClienteService extends AbstractCrudService<Cliente, Long, ClienteDAO>{
    private ClienteDAO clienteDAO;
    
    {
    clienteDAO=new ClienteDAO();
    }
    
    public ClienteDAO getDAO(){
        return clienteDAO;
    }
}
