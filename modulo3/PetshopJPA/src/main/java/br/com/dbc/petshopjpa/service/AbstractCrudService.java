/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.AbstractDAO;
import br.com.dbc.petshopjpa.dao.ClienteDAO;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.List;
import static javafx.scene.input.KeyCode.I;

/**
 *
 * @author eduardo.barbosa
 */
public abstract class AbstractCrudService<E, I, DAO extends AbstractDAO<E, I>> {
    
    public abstract DAO getDAO();
    
     
    public List<E> findAll(){
        return getDAO().findAll();
    }
    public E findOne(I id){
        return getDAO().findOne(id);
    }
    public void create(E entidade){
         getDAO().create(entidade);
    }
     public void update(E entidade){
         getDAO().update(entidade);
    }
     public void delete(I id){
         getDAO().delete(id);
    }
}
