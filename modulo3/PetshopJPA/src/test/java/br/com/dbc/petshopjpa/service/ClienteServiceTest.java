/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.service;

import br.com.dbc.petshopjpa.dao.ClienteDAO;
import br.com.dbc.petshopjpa.dao.PersistenceUtils;
import br.com.dbc.petshopjpa.entity.Animal;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;

/**
 *
 * @author eduardo.barbosa
 */
public class ClienteServiceTest {
    
    private final EntityManager em = PersistenceUtils.getEntityManager();
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        em.getTransaction().begin();
        em.createQuery("delete from Cliente").executeUpdate();
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        ClienteService instance = new ClienteService();
        em.getTransaction().begin();
        Cliente cliente = Cliente.builder()
                .nome("Testador")
                .animalList(Arrays.asList(Animal
                .builder()
                .nome("Pé de Feijão")
                .build()))
         .build();
        em.persist(cliente);
        em.getTransaction().commit();
        List<Cliente> clientes = instance.findAll();
        
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(cliente.getId(), clientes.stream().findFirst().get().getId());
        Assert.assertEquals(cliente.getAnimalList().size(), 
                clientes.stream().findFirst().get().getAnimalList().size());
        
       // Assert.assertEquals();
        // fail("The test case is a prototype.");
    }
    
    public void testFindOneMocked(){
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente created  = Cliente.builder()
                .id(1L)
                .nome("Testador")
                .animalList(Arrays
                        .asList(Animal.builder()
                        .id(1L)
                        .nome("Velho")
                        .build()))
                .build();
        
    }

    
}
