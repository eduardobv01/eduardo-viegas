package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Aluguel;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.dbc.locadora.repository.AluguelRepository;

@Service
@Transactional( readOnly = true, rollbackFor = Exception.class )
public class AluguelService {

    @Autowired
    private AluguelRepository aluguelRepository;
    
    //save
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Aluguel save( Aluguel aluguel ){
        return aluguelRepository.save(aluguel);
    }
    
    //delete
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete( Long id ){
        aluguelRepository.deleteById(id);
    }
    
    //findById
    public Optional<Aluguel> findById( Long id ){
        return aluguelRepository.findById(id);
    }

    //findAll
    public Page<Aluguel> findAll( Pageable pa ){
        return aluguelRepository.findAll( pa );
    }
    
}
