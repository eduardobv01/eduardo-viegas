package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Aluguel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AluguelRepository extends JpaRepository<Aluguel, Long> {
    
}
