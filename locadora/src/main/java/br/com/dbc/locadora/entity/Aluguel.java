package br.com.dbc.locadora.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Aluguel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String descricao;//remover
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate retirada;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate previsao;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate devolucao;
    
    private Double multa;
    
    @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Cliente idCliente;
}
