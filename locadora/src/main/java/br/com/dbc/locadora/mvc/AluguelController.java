package br.com.dbc.locadora.mvc;

import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.service.AluguelService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AluguelController {
        
    @Autowired
    private AluguelService aluguelService;
    
    @RequestMapping("/")
    public String page(Model model) {
        List<Aluguel> alugueis = aluguelService
            .findAll(PageRequest.of(0,20)).getContent();
        model.addAttribute("alugueis", alugueis);
        return "apolice";
    }
    
    @PostMapping("/")
    public String save(Aluguel aluguel, Model model) {
        aluguelService.save(aluguel);
        List<Aluguel> alugueis = aluguelService
            .findAll(PageRequest.of(0,20)).getContent();
        model.addAttribute("alugueis", alugueis);
        return "alugueis";
    }

}
