package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.service.AluguelService;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.data.domain.Pageable;


@RestController
@RequestMapping("/cliente")
public class ClienteRestController {
    
    @Autowired
    private AluguelService aluguelService;
    
    @GetMapping()
    public ResponseEntity<?> list(Pageable pageable) {
        return ResponseEntity.ok( aluguelService.findAll( pageable ) );
    }
    
    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable Long id) {
        return aluguelService.findById(id)
                .map(ResponseEntity::ok)
                .orElse( ResponseEntity.notFound().build() );
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody Aluguel aluguel) {
        if(id == null || !Objects.equals(aluguel.getId(), id))
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(aluguelService.save(aluguel));
    }
    
    @PostMapping
    public ResponseEntity<?> post(@RequestBody Aluguel input) {
        if (input.getId() != null)
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(aluguelService.save(input));
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        aluguelService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error message")
    public void handleError() {
    }
    
}
