describe( 'concatenarEmPaz', function() {
  const expect = chai.expect
  beforeEach( function() {
    chai.should()
  } )

  it( 'passar undefined primeiro parâmetro', function() {
    concatenarEmPaz( undefined, 'Soja' ).should.equal( 'Soja' )
  } )

  it( 'passar null segundo parâmetro', function() {
    concatenarEmPaz( 'Soja é bom, mas...', null ).should.equal( 'Soja é bom, mas...' )
  } )

  it( 'passar texto nos dois parâmetros', function() {
    concatenarEmPaz( 'Soja', ' é muito bom' ).should.equal( 'Soja é muito bom' )
  } )

  it( 'passar undefined no segundo parâmetro', function() {
    concatenarEmPaz( 'Soja é' ).should.equal( 'Soja é' )
  } )

  it( 'passar null nos dois parâmetros', function() {
    concatenarEmPaz( null, null ).should.equal( '' )
  } )

  it( 'passar undefined nos dois parâmetros', function() {
    concatenarEmPaz().should.equal( '' )
  } )
} )
