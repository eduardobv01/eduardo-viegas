describe( 'fiboSum', function() {
  const expect = chai.expect
  beforeEach( function() {
    chai.should()
  } )

  it( '1 => 1', function() {
    fiboSum( 1 ).should.equal( 1 )
  } )

  it( '5 => 12', function() {
    fiboSum( 5 ).should.equal( 12 )
  } )

  it( '7', function() {
    fiboSum( 7 ).should.equal( 33 )
  } )
} )
