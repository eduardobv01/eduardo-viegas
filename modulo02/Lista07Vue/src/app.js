const urlApi = 'https://pokeapi.co/api/v2/pokemon'
const pokeApi = new PokeApi( urlApi )

let app = new Vue( {
  el: '#meuPrimeiroApp',
  data: {
    idParaBuscar: '',
    pokemon: { }
  },
  methods: {
    async buscar() {
      this.pokemon = await pokeApi.buscar( this.idParaBuscar )
    },
    async estouComSorte() {
      var aleatorio=""
      while (localStorage.aleatorio.includes(aleatorio)) {
        aleatorio = Math.floor((Math.random() * 802) + 1);
    }
    localStorage.aleatorio += aleatorio + " "
      this.pokemon = await pokeApi.buscar(aleatorio)
    }
  }
} )
