class Pokemon {
    constructor( jsonVindoDaApi ) {
      this.nome = jsonVindoDaApi.name
      this.id = jsonVindoDaApi.id
      this.thumbUrl = jsonVindoDaApi.sprites.front_default
      this._altura = jsonVindoDaApi.height
      this._peso = jsonVindoDaApi.weight
      this.tipos = jsonVindoDaApi.types.map( t => t.type.name )
      this.stats = jsonVindoDaApi.stats.map( t => {
        return { 
          nome: t.stat.name, 
          valor: t.base_stat
        }
      } )
    }
  
    get altura() {
      return this._altura * 10
    }
    get peso() {
      return this._peso/10
    }
  }
  