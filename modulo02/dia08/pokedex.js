<<<<<<< HEAD
function rodarPrograma() {

  // obtém elementos da tela para trabalhar com eles
  const $dadosPokemon = document.getElementById( 'dadosPokemon' )
  const $h1 = $dadosPokemon.querySelector( 'h1' )
  const $img = $dadosPokemon.querySelector( '#thumb' )
  const $txtIdPokemon = document.getElementById( 'txtNumero' )
  // instanciando objeto de API que criamos para facilitar a comunicação
  const url = 'https://pokeapi.co/api/v2/pokemon'
  const pokeApi = new PokeApi( url )

  // registrar evento de onblur
  $txtIdPokemon.onblur = function() {
    const idDigitado = $txtIdPokemon.value
    pokeApi.buscar( idDigitado )
      .then( res => res.json() )
      .then( dadosJson => {
        // criar objeto pokémon e renderiza-lo na tela
        const pokemon = new Pokemon( dadosJson )
        renderizarPokemonNaTela( pokemon )
      } )
  }

  function renderizarPokemonNaTela( pokemon ) {
    $h1.innerText = pokemon.nome
    $img.src = pokemon.thumbUrl
  }

}

rodarPrograma()
=======

var sorte = document.getElementById("sorte")
sorte.addEventListener("click", estouComSorte);

function rodarPrograma(consulta){
    var campo = document.getElementById("campo")
    if (isNaN(parseInt(consulta))) {
        campo.value = "";
    }
   const $dadosPokemon = document.getElementById("dadosPokemon")
   const $h1 = $dadosPokemon.querySelector("h1")
   const $img = $dadosPokemon.querySelector("#thumb")
   let url =`https://pokeapi.co/api/v2/pokemon/${consulta}/`
    
    fetch(url)
    .then(res=>res.json())//Retorna um json (também abrindo uma promisse, porque também é assícrono)
    .then(dadosJson=>{
        $h1.innerText=dadosJson.name
        $img.src=dadosJson.sprites.front_default
    })
}

function estouComSorte(){
    var aleatorio = Math.floor((Math.random() * 802) + 1);
    const $dadosPokemon = document.getElementById("dadosPokemon")
    const $h1 = $dadosPokemon.querySelector("h1")
    const $img = $dadosPokemon.querySelector("#thumb")
    let url =`https://pokeapi.co/api/v2/pokemon/${aleatorio}/`
     
     fetch(url)
     .then(res=>res.json())
     .then(dadosJson=>{
         $h1.innerText=dadosJson.name
         $img.src=dadosJson.sprites.front_default
     })
}
//É executado enquanro os thens são executados
//Função se autochamando - O mesmo que criar normalmente e depois chamar
>>>>>>> master
