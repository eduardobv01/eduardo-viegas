describe( 'Exercicio 02', function() {
  const expect = chai.expect


  beforeEach( function() {
    chai.should()
  } )

    it( 'Nome do primeiro elfo deve ser LEGOLAS e do segundo GALADRIEL', function() {
      const elfo1 = {
        nome: "Legolas",
        experiencia: 0,
        qtdFlechas: 6
      }

      const elfo2 = {
        nome: "Galadriel",
        experiencia: 1,
        qtdFlechas: 1
      }

      const arrayElfos = [ elfo1, elfo2 ]
      formatarElfos(arrayElfos)
      var resultado = arrayElfos[0].nome;
      resultado.should.equal("LEGOLAS")
      resultado = arrayElfos[1].nome;
      resultado.should.equal("GALADRIEL")
      } )

      it( 'O atributo experiencia deve ser undefined para ambos os Elfos', function() {
        const elfo1 = {
          nome: "Legolas",
          experiencia: 0,
          qtdFlechas: 6
        }
          const elfo2 = {
            nome: "Galadriel",
            experiencia: 1,
            qtdFlechas: 1
          }
          const arrayElfos = [ elfo1, elfo2 ]
          formatarElfos(arrayElfos)
          var resultado = typeof arrayElfos[0].experiencia
          resultado.should.equal("undefined")
          resultado = typeof arrayElfos[1].experiencia
          resultado.should.equal("undefined")
        } )


      it( 'temExperiencia deve ser false se a Experiência for negativa', function() {
      const elfo1 = {
        nome: "Legolas",
        experiencia: 0,
        qtdFlechas: 6
      }
        const elfo2 = {
          nome: "Galadriel",
          experiencia: -10,
          qtdFlechas: 1
        }
        const arrayElfos = [ elfo1, elfo2 ]
        formatarElfos(arrayElfos)
        var resultado = arrayElfos[0].temExperiencia
        resultado.should.equal(false)
    } )

    it( 'A descrição deve corresponder com os dados do objeto', function() {
    const elfo1 = {
      nome: "Legolas",
      experiencia: 0,
      qtdFlechas: 0
    }
      const elfo2 = {
        nome: "Galadriel",
        experiencia: 1,
        qtdFlechas: 1
      }
      const arrayElfos = [ elfo1, elfo2 ]
      formatarElfos(arrayElfos)
      var descricaoEsperada = elfo1.nome.toUpperCase() + " sem experiência e sem flechas"
      var resultado = arrayElfos[0].descricao;
      resultado.should.equal(descricaoEsperada)
      descricaoEsperada = elfo2.nome.toUpperCase() + " com experiência e com " + elfo2.qtdFlechas + " flechas"
      resultado = arrayElfos[1].descricao;
      resultado.should.equal(descricaoEsperada)
     } )

     /*it( 'Array de elfos retornado deve ser igual ao esperado', function() {
      const elfo1 = {
        nome: "Legolas",
        experiencia: 0,
        qtdFlechas: 6
      }
        const elfo2 = {
          nome: "Galadriel",
          experiencia: 1,
          qtdFlechas: 1
        }
        const arrayElfos = [ elfo1, elfo2 ]
        var arrayEsperado = arrayElfos;
        formatarElfos(arrayElfos)
        resultado.should.equal(arrayEsperado)
       } )*/

} )
