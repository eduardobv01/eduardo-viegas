describe( 'somarPosicaoPares', function() {
  const expect = chai.expect

  // antes de cada teste, chama a biblioteca "chai" para poder usar as asserções de forma mais amigável (should, expect)
  // documentação: https://www.chaijs.com/
  beforeEach( function() {
    chai.should()
  } )
    // cada bloco "it" é um cenário de teste, como se fosse um @Test do JUnit
    // importante: garanta que o arquivo onde o código está implementado foi incluído, em rodar-testes.html
    it( 'Deve somar apenas as posições pares', function() {
      var resultado = somarPosicaoPares([1,1,1,1])
      resultado.should.equal(2)
    } )

    it( 'Soma de números negativos deve ser negativo', function() {
      var resultado = somarPosicaoPares([-1,-1,-1,-1])
      resultado.should.equal(-2)
    } )
    
    it( 'Soma de números decimais', function() {
      var resultado = somarPosicaoPares([-1.5, -2.5,-1.1,-1.7])
      resultado.should.equal(-2.6)
    } )

    it( '[] deve retornar 0', function() {
      var resultado = somarPosicaoPares([])
      resultado.should.equal(0)
    } )
} )
