function formatarElfos(arrayElfos){
  for(var i=0; i<arrayElfos.length; i++){
    arrayElfos[i].nome = arrayElfos[i].nome.toUpperCase()
    arrayElfos[i].temExperiencia = arrayElfos[i].experiencia>0
    var exp = arrayElfos[i].temExperiencia? "com":"sem"
    var flechas = arrayElfos[i].qtdFlechas>0? "com " + arrayElfos[i].qtdFlechas:"sem"
    arrayElfos[i].descricao=`${arrayElfos[i].nome} ${exp} experiência e ${flechas} flechas`
    delete arrayElfos[i].experiencia
  }
  return arrayElfos;
}
