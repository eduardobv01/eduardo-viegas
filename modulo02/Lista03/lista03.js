var raio=1, tipoCalculo='A';

/*var instrucoes = {//Pattern Matching
  raio,
  tipoCalculo
}*/

calcularCirculo({raio, tipoCalculo});

function calcularCirculo(instrucoes) {
     if(instrucoes["tipoCalculo"]==="A"){
        return Math.PI * Math.pow(instrucoes.raio, 2);
     }else if (instrucoes["tipoCalculo"]==="C") {
        return 2 * Math.PI * instrucoes.raio;
     }
}

function naoBissexto(ano) {
  if (ano%4==0 && (ano%100!=0 || ano%400==0)){
    return false;
  }else {
    return true;
  }
}

function concatenarSemUndefined(string1, string2) {
  if(typeof string1==="undefined" && typeof string2==="undefined"){
    return "";
  }
  if(typeof string2==="undefined"){
    return string1;
  }
  if(typeof string1==="undefined"){
    return string2;
  }
  return string1.concat(string2);
}
function concatenarSemUndefined(texto1="",texto2=""){
  //return texto1+texto2; Ou pode-se usar interpolação de Strings
  return `${texto1}${texto2}`; 
}

function concatenarSemNull(string1, string2) {
  if(string1==null && string2==null){
    return "";
  }
  if(string2===null){
    return string1;
  }
  if(string1===null){
    return string2;
  }
  return string1.concat(string2);
}

function concatenarEmPaz(string1, string2) {
  if(string1==null && string2==null||string1=="undefined" && string2=="undefined"){
    return "";
  }
  if(string2===null||typeof string2=="undefined"){
    return string1;
  }
  if(string1===null||typeof string1=="undefined"){
    return string2;
  }
  return string1.concat(string2);
}

function adicionar(valor1) {
  return function(valor2){
    return valor1+valor2;
  }
}

function fiboSum(numero){
  
  function fribonacci(numero) {
    if(numero<2){
      return 1;
    }
    return fribonacci(numero-1)+fribonacci(numero-2);
 }

  var soma=0;
  for(var i=0; i<numero; i++){
    soma+=fribonacci(i);
  }
  return soma;
}


