describe( 'calcularCirculo', function() {
    //const expect = chai.expect;
    // antes de cada teste, chama a biblioteca "chai" para poder usar as asserções de forma mais amigável (should, expect)
    // documentação: https://www.chaijs.com/
    beforeEach( function() {
      chai.should()
    } )
  
    // cada bloco "it" é um cenário de teste, como se fosse um @Test do JUnit
    // importante: garanta que o arquivo onde o código está implementado foi incluído, em rodar-testes.html
    it( 'deve calcular área de raio 1', function() {
      const raio = 1, tipoCalculo = 'A'//Arrange
      const resultado = calcularCirculo( { raio, tipoCalculo } )//Act
      resultado.should.equal( Math.PI )//Assert 
      //Monkey patch
    } )
  
    // isso é um cenário pendente, é preciso implementar o código no segundo parâmetro, que é uma function.
    it( 'deve calcular circunferência de raio 1' )
  } )
  