
var sorte = document.getElementById("sorte")
sorte.addEventListener("click", estouComSorte);
var criar = true;
var aleatorio = ""
localStorage.aleatorio = ""
const $dadosPokemon = document.getElementById("dadosPokemon")

function rodarPrograma(consulta) {
    var id = document.getElementById("campo")
    if (isNaN(parseInt(consulta))) {
        id.value = "";
    }

    if (id.value != localStorage.id) {
        localStorage.id = id.value
        const $h1 = $dadosPokemon.querySelector("h1")
        const $img = $dadosPokemon.querySelector("#thumb")
        const $celulasTabela = $dadosPokemon.getElementsByTagName("td")
        const $especificacoes = document.getElementById("especificacoes")
        const $estatisticas=document.getElementById("estatisticas")
        let url = `https://pokeapi.co/api/v2/pokemon/${consulta}/`

        fetch(url)
            .then(res => res.json())//Retorna um json (também abrindo uma promisse, porque também é assícrono)
            .then(dadosJson => {
                $h1.innerText = dadosJson.name
                $img.src = dadosJson.sprites.front_default
                $celulasTabela[1].innerText = dadosJson.id
                $celulasTabela[3].innerText = dadosJson.height * 10 + "cm"
                $celulasTabela[5].innerText = dadosJson.weight / 10 + "kg"
                var tipos = "";
                dadosJson.types.forEach((elemento, i) => {
                    tipos += dadosJson.types[i].type.name + ", "
                })
                $celulasTabela[7].innerHTML = tipos.substr(0, (tipos.length - 2))
                var $tr, $td;
                //if (criar) {
                    while($estatisticas.firstChild){
                        $estatisticas.removeChild($estatisticas.firstChild)
                    }
                    dadosJson.stats.forEach((elemento, i) => {//Neste caso
                        $tr = document.createElement("tr")
                        $td = [document.createElement("td"), document.createElement("td")]
                        $td[0].innerText = dadosJson.stats[i].stat.name
                        $td[1].innerText = dadosJson.stats[i].base_stat
                        $tr.appendChild($td[0])
                        $tr.appendChild($td[1])
                        $estatisticas.appendChild($tr)
                    })
                   // criar = false;
                /*}else{
                    dadosJson.stats.forEach((elemento, i) => {//Neste caso
                        $tr = document.getElementsByTagName("tr")
                        //alert($tr[6].getElementsByTagName("td")[0].innerHTML)
                        $td = $tr[i].getElementsByTagName("td")
                        alert($td[8].innerText)
                        $td[0].innerText = "atributo1"//adosJson.stats[i].stat.name
                        $td[1].innerText = "atributo2"//dadosJson.stats[i].base_stat
                        //$tr.appendChild($td[0])
                        //$tr.appendChild($td[1])
                    })*/
                //}

            })
    }
}

function estouComSorte() {
    while (localStorage.aleatorio.includes(aleatorio)) {
        aleatorio = Math.floor((Math.random() * 802) + 1);
    }
    localStorage.aleatorio += aleatorio + " "
    const $dadosPokemon = document.getElementById("dadosPokemon")
    const $h1 = $dadosPokemon.querySelector("h1")
    const $img = $dadosPokemon.querySelector("#thumb")
    let url = `https://pokeapi.co/api/v2/pokemon/${aleatorio}/`

    fetch(url)
        .then(res => res.json())
        .then(dadosJson => {
            $h1.innerText = dadosJson.name
            $img.src = dadosJson.sprites.front_default
        })
}
//É executado enquanro os thens são executados
//Função se autochamando - O mesmo que criar normalmente e depois chamar