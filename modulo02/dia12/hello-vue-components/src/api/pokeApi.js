import Pokemon from '../models/pokemon.js'

export default class PokeApi {
  constructor( url ) {
    this.url = url
  }

  // const pokemonsDoTipo = await pokeApi.listarPorTipo( .. )
<<<<<<< HEAD
  async listarPorTipo( idTipo, qtdPagina, pagina ) {
=======
  async listarPorTipo( idTipo, qtdPagina, pagina) {
>>>>>>> master
    return new Promise( resolve => {
      const urlTipo = `https://pokeapi.co/api/v2/type/${ idTipo }/`
      fetch( urlTipo )
        .then( j => j.json() )
        .then( t => {
<<<<<<< HEAD
          const pokemons = t.pokemon.slice( 0, qtdPagina )
=======
          const pokemons = typeof pagina!= 'undefined'? t.pokemon.slice( pagina, qtdPagina ):t.pokemon.slice(0, qtdPagina )
>>>>>>> master
          const promisesPkm = pokemons.map( p => this.buscarPorUrl( p.pokemon.url ) )
          Promise.all( promisesPkm ).then( resultadoFinal => {
            resolve( resultadoFinal )
          } )
        } )
    } )
  }

  async buscarPorUrl( urlPokemon ) {
    return new Promise( resolve => {
      fetch( urlPokemon )
        .then( j => j.json() )
        .then( p => {
          const pokemon = new Pokemon( p )
          // return "assícrono",
          // vai jogar o valor de pokemon para quem chamou utilizá-lo de forma assíncrona
          resolve( pokemon )
        } )
    } )
  }

  async buscar( idPokemon ) {
    let urlPokemon = `${ this.url }/${ idPokemon }/`
    return this.buscarPorUrl( urlPokemon )
  }
}
