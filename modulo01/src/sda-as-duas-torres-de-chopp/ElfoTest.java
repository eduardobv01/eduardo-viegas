import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class ElfoTest {
<<<<<<< HEAD

    private final double DELTA = 0.1;

    @After
    public void tearDown() {
        System.gc();
    }

=======
    
    private final double DELTA = 0.1;
    
>>>>>>> master
    @Test
    public void criarElfoInformandoNome() {
        Elfo legolas = new Elfo("Legolas");
        assertEquals("Legolas", legolas.getNome());
    }   

    @Test
    public void celebornAtiraUmaFlecha() {
        Elfo celeborn = new Elfo("Celeborn");
        celeborn.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(6, celeborn.getFlecha().getQuantidade());
        assertEquals(1, celeborn.getExperiencia());
    }

    @Test
    public void legolasTentaAtirarOitoFlechas() {
        Elfo elfo = new Elfo("Legolas");
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        elfo.atirarFlecha(new Dwarf("Gimli"));
        assertEquals(0, elfo.getFlecha().getQuantidade());
        assertEquals(7, elfo.getExperiencia());
    }

    @Test
    public void atirarFlechaEmUmDwarf() {
        Elfo legolas = new Elfo("Legolas");
        Dwarf gimli = new Dwarf("Gimli");
        legolas.atirarFlecha(gimli);
        assertEquals(6, legolas.getFlecha().getQuantidade());
        assertEquals(1, legolas.getExperiencia());
        assertEquals(100.0, gimli.getVida(), DELTA);
    }
<<<<<<< HEAD

=======
    
>>>>>>> master
    @Test
    public void elfoNasceComArcoEFlecha() {
        Elfo elfo = new Elfo("Legolas");
        ArrayList<Item> obtido = elfo.getInventario().getItens();
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
<<<<<<< HEAD
                    new Item("Arco", 1),
                    new Item("Flecha", 7)
                ));
        assertEquals(esperado, obtido);
    }

    /*@Test
    public void criarUmElfoIncrementaContadorUmaVez() {
        new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }

    @Test
    public void criarDoisElfosIncrementaContadorDuasVezes() {
        new Elfo("Legolas");
        new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }

    @Test
    public void naoCriarElfoNaoIncrementaContador() {
        new Dwarf("Balin");
        assertEquals(0, Elfo.getQtdElfos());        
    }*/
}


=======
            new Item("Arco", 1),
            new Item("Flecha", 7)
        ));
        assertEquals(esperado, obtido);
    }
}
>>>>>>> master
