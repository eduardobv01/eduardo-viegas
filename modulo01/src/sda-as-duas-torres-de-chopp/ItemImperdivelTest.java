import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemImperdivelTest {
    @Test
    public void itemImperdivelNaoPodeSerZerado() {
        Item item = new ItemImperdivel("Anduril", 1);
        item.setQuantidade(0);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemImperdivelNaoPodeSerNegativo() {
        Item item = new ItemImperdivel("Anduril", 1);
        item.setQuantidade(-10);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemImperdivelPodeReceberOutraQuantidade() {
        Item item = new ItemImperdivel("Escudo de madeira", 2);
        item.setQuantidade(1);
        assertEquals(1, item.getQuantidade());
    }

    @Test
    public void itemImperdivelNaoPodeSerCriadoComZero() {
        Item item = new ItemImperdivel("Escudo de madeira", 0);
        assertEquals(1, item.getQuantidade());
    }
}
