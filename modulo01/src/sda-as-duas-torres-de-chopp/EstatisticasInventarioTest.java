<<<<<<< HEAD
=======


>>>>>>> master
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

<<<<<<< HEAD
public class EstatisticasInventarioTest {

    private final double DELTA = 0.1;

    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertTrue(Double.isNaN(estatisticas.calcularMedia()));
        // assertNull(estatisticas.calcularMedia());
    }

    @Test
    public void calcularMediaInventarioUmElemento() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2, estatisticas.calcularMedia(), DELTA);
    }

    @Test
    public void calcularMediaInventarioQtdsIguais() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 4));
        inventario.adicionar(new Item("Flechas de fogo", 4));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(4, estatisticas.calcularMedia(), DELTA);
    }

    @Test
    public void calcularMediaInventarioQtdsDiferentes() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 4));
        inventario.adicionar(new Item("Flechas de fogo", 4));
        inventario.adicionar(new Item("Anduril", 1));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.calcularMedia(), DELTA);
    }

    @Test
    public void calcularMedianaComInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertTrue(Double.isNaN(estatisticas.calcularMediana()));
        // assertNull(estatisticas.calcularMediana());
    }

    @Test
    public void calcularMedianaComUmItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Lembas", 5));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(5, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void calcularMedianaComQtdParDeItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Lembas", 5));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3.5, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void calcularMedianaComQtdImparDeItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Lembas", 5));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void calcularMedianaComItensDesordenados() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item("Lembas", 5));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3, estatisticas.calcularMediana(), DELTA);
    }

    @Test
    public void qtdItensAcimaDaMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }

    @Test
    public void qtdItensAcimaDaMediaComUmItem() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item("Arco de vidro", 1));
        assertEquals(0, estatisticas.qtdItensAcimaDaMedia());
    }

    @Test
    public void qtdItensAcimaDaMediaComVariosItens() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item("Lembas", 5));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Flechas de fogo", 2));
        inventario.adicionar(new Item("Clava de madeira", 6));
        assertEquals(3, estatisticas.qtdItensAcimaDaMedia());
    }

    @Test
    public void qtdItensAcimaDaMediaComItemIgualAMedia() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item("Lembas", 5));
        inventario.adicionar(new Item("Escudo de aluminio", 2));
        inventario.adicionar(new Item("Anduril", 1));
        inventario.adicionar(new Item("Bracelete", 4));
        inventario.adicionar(new Item("Flechas de fogo", 3));
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
=======

public class EstatisticasInventarioTest{
    
    @Test
    public void medianaDeveCorresponderAos5ItensAdicionados(){
    Inventario mochila = new Inventario();
    EstatisticasInventario dados = new EstatisticasInventario(mochila);
    Item anel = new Item("Anel de Sauron", 1);
    Item flecha = new Item("Flecha", 10);
    Item arco = new Item("Arco", 1);
    Item mapa = new Item("Mapa", 5);
    Item adaga = new Item("Adaga Daedra", 2);
    mochila.adicionar(anel);
    mochila.adicionar(flecha);
    mochila.adicionar(arco);
    mochila.adicionar(mapa);
    mochila.adicionar(adaga);
    assertEquals(adaga.getQuantidade(), dados.calcularMediana(), 0.1);
    }
    @Test
    public void medianaDeveCorresponderAos8ItensAdicionados(){
    Inventario mochila = new Inventario();
    EstatisticasInventario dados = new EstatisticasInventario(mochila);
    Item anel = new Item("Anel de Sauron", 1);
    Item arco = new Item("Arco", 1);
    Item adaga = new Item("Adaga Daedra", 2);
    Item elixir = new Item("Elixir dos Pes Ligeiros", 2);
    Item hidromel = new Item("Hidromel", 3);
    Item dentes = new Item("Dentes de Orc", 4);
    Item mapa = new Item("Mapa", 5);
    Item flecha = new Item("Flecha", 10);
    mochila.adicionar(anel);
    mochila.adicionar(flecha);
    mochila.adicionar(arco);
    mochila.adicionar(mapa);
    mochila.adicionar(adaga);
    mochila.adicionar(elixir);
    mochila.adicionar(arco);
    mochila.adicionar(mapa);
    assertEquals((elixir.getQuantidade()+hidromel.getQuantidade())/2, dados.calcularMediana(), 0.1);
    }
    @Test
    public void quantidadeDeItensAcimaDaMedia(){//Media=4,5
    Inventario mochila = new Inventario();
    EstatisticasInventario dados = new EstatisticasInventario(mochila);
    Item anel = new Item("Anel de Sauron", 1);
    Item arco = new Item("Arco", 1);
    Item adaga = new Item("Adaga Daedra", 2);
    Item elixir = new Item("Elixir dos Pes Ligeiros", 4);
    Item hidromel = new Item("Hidromel", 5);//Acima
    Item dentes = new Item("Dentes de Orc", 6);//Acima
    Item mapa = new Item("Mapa", 7);//Acima
    Item flecha = new Item("Flecha", 10);//Acima
    mochila.adicionar(anel);
    mochila.adicionar(flecha);
    mochila.adicionar(arco);
    mochila.adicionar(mapa);
    mochila.adicionar(adaga);
    mochila.adicionar(elixir);
    mochila.adicionar(hidromel);
    mochila.adicionar(dentes);
    assertEquals(4, dados.qtdItensAcimaDaMedia(), 0.1);
>>>>>>> master
    }
}
