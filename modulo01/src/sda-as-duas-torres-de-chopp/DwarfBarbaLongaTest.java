import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {

    private final double DELTA = 0.1;

    @Test
    public void dwarfDevePerderVida66Percent() {
        DadoFake dadoFalso = new DadoFake();
        dadoFalso.simularValor(4);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.perderVida();
        assertEquals(100.0, balin.getVida(), DELTA);
    }

    @Test
    public void dwarfNaoDevePerderVida33Percent() {
        DadoFake dadoFalso = new DadoFake();
        dadoFalso.simularValor(5);
        Dwarf balin = new DwarfBarbaLonga("Balin", dadoFalso);
        balin.perderVida();
        assertEquals(110.0, balin.getVida(), DELTA);
    }
}
