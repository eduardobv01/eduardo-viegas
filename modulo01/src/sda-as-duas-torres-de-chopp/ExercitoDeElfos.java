<<<<<<< HEAD
import java.util.*;

public class ExercitoDeElfos {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfoVerde.class,
                ElfoNoturno.class
            )
        );
    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>(); 

    public void alistar(Elfo elfo) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if (podeAlistar) {
            elfos.add(elfo);
            ArrayList<Elfo> elfosDoStatus = porStatus.get(elfo.getStatus());
            if (elfosDoStatus == null) {
                elfosDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfosDoStatus);
            }
            elfosDoStatus.add(elfo);
        }
    }

    public ArrayList<Elfo> buscar(Status status) {
        return this.porStatus.get(status);
    }

    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
}

=======
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Arrays;

public class ExercitoDeElfos{//
    private HashMap<String, Elfo> exercito = new HashMap<>();
    private HashMap<Status, ArrayList<Elfo>> elfosVivosEMortos = new HashMap<>();//IDE - Indice de desenvolvimento Elfico
    
    public void alistarElfo(Elfo elfo){
        if (elfo instanceof ElfoVerde || elfo instanceof ElfoNoturno) {
            exercito.put(elfo.getNome(), elfo);
        }
    }
    public void registrarStatus() {
        for (Map.Entry<String,Elfo> elfoIntegrante : exercito.entrySet()) {
             Status status = elfoIntegrante.getValue().getStatus();
             if(elfosVivosEMortos.get(status)!=null) elfosVivosEMortos.get(status).add(elfoIntegrante.getValue());
             else elfosVivosEMortos.put(status, new ArrayList(Arrays.asList(elfoIntegrante.getValue())));    
        }
    }
    public Elfo getElfoDoExercito(String nome){
    return this.exercito.get(nome);
    }
    public ArrayList<Elfo> buscarElfosDeUmStatus(Status status){
        return elfosVivosEMortos.get(status);
    }
}
>>>>>>> master
