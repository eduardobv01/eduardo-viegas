public class Item extends Object {
<<<<<<< HEAD
    private String descricao;
=======
    protected String descricao;
>>>>>>> master
    protected int quantidade;

    public Item(String descricao, int quantidade) {
        this.descricao = descricao;
        this.setQuantidade(quantidade);
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void setQuantidade(int novaQuantidade) {
        this.quantidade = novaQuantidade;
    }
    
    // espada.equals(outraEspada);
    @Override
    public boolean equals(Object outroItem) {
        Item outro = (Item)outroItem;
        
        boolean iguais = 
            this.descricao.equals(outro.descricao)
            && this.quantidade == outro.quantidade;
        return iguais;
    }
}