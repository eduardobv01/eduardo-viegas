
public class EspadaDeGalvorn extends Item{
    public EspadaDeGalvorn(String descricao, int quantidade){
    super(descricao, quantidade<=0? 1:quantidade);
    
    }
   
    public void setQuantidade(int novaQuantidade){
        if(novaQuantidade>0){
            this.quantidade=novaQuantidade;
        }else{
            this.quantidade=1;
        }
    }
    
    
    
   
}
