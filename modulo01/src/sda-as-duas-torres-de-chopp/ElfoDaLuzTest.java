import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ElfoDaLuzTest {

    private static final double DELTA = 0.1;

<<<<<<< HEAD
    @After
    public void tearDown() {
        System.gc();
    }

=======
>>>>>>> master
    @Test
    public void elfoDaLuzNasceComEspada() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7),
                    new Item("Espada de Galvorn", 1)
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzDevePerderVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        //ElfoDaLuz feanorDaLuz = (ElfoDaLuz)feanor;
        //((ElfoDaLuz)feanor).atacarComEspada(new Dwarf(""));
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        assertEquals(79, feanor.getVida(), DELTA);
        assertEquals(100, gul.getVida(), DELTA);
    }

    @Test
    public void elfoDaLuzDeveGanharVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        //ElfoDaLuz feanorDaLuz = (ElfoDaLuz)feanor;
        //((ElfoDaLuz)feanor).atacarComEspada(new Dwarf(""));
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89, feanor.getVida(), DELTA);
        assertEquals(90, gul.getVida(), DELTA);
    }

    @Test
    public void elfoDaLuzMorreAoAtacarTantasVezes() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        for (int i = 0; i < 17; i++) {
            feanor.atacarComEspada(gul);
        }
        assertEquals(0, feanor.getVida(), DELTA);
        assertEquals(0, gul.getVida(), DELTA);
        assertTrue(feanor.estaMorto());
        assertTrue(gul.estaMorto());
    }

    @Test
    public void elfoDaLuzNaoPodePerderEspadaDeGalvorn() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item("Espada de Galvorn", 1));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Arco", 1),
                    new Item("Flecha", 7),
                    new Item("Espada de Galvorn", 1)
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzPodePerderArco() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item("Arco", 1));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item("Flecha", 7),
                    new Item("Espada de Galvorn", 1)
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }
<<<<<<< HEAD

=======
    
>>>>>>> master
    @Test
    public void elfoDaLuzSoAtacaComUnidadeDeEspada() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getInventario().getItens().get(2).setQuantidade(0);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(79, feanor.getVida(), DELTA);
    }
}






<<<<<<< HEAD
=======






>>>>>>> master
