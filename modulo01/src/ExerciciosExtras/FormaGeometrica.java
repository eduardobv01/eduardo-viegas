public interface FormaGeometrica {
    void setX(int x);
    void setY(int y);
    int calcularArea();
}
