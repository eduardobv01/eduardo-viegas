import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class AgendaContatos{
    private LinkedHashMap<String, String> listaTelefonica = new LinkedHashMap<String, String>();;

    public void adicionarContato(String nome, String numero){
        if(nome != null && numero != null && listaTelefonica.get(nome)==null)//Keep it simple
        listaTelefonica.put(nome,numero);
    }
    public String obterTelefone(String nome){
        return listaTelefonica.get(nome);
    }
    public String obterNome(String telefone){
        for (Map.Entry<String,String> contato : listaTelefonica.entrySet()) {
             if(contato.getValue().equalsIgnoreCase(telefone)) 
             return contato.getKey();
        }  
        return null;
    }
    public String csv(){//Refatorar baseando-se no String builder visto em aula
        String csv = new String();
        for (Map.Entry<String,String> contato : listaTelefonica.entrySet()) {
             csv+=contato.getKey() + "," + contato.getValue();
        }  
        return csv.substring(0, csv.length()-1);
    } 
}

