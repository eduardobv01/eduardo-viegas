

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AgendaContatosTest{
    
    private AgendaContatos agenda = new AgendaContatos();
    //Testes iniciais
    @Test
    public void agendaDeveAdicionarContato(){
    agenda.adicionarContato("Bernardo", "92342304");
    assertEquals("92342304", agenda.obterTelefone("Bernardo"));
    }
    @Test
    public void agendaDeveBuscarPorTelefone(){
    agenda.adicionarContato("Bernardo", "92342304");
    assertEquals("Bernardo", agenda.obterNome("92342304"));
    }
    @Test
    public void agendaDeveGerarCSV(){
    String separador = System.lineSeparator();
    agenda.adicionarContato("Bernardo", "92342304");
    agenda.adicionarContato("Odranreb", "40324329");
    String esperado = String.format("Bernardo,92342304%sOdranreb,40324329%s", separador, separador);
    assertEquals(esperado, agenda.csv());
    } 
}
