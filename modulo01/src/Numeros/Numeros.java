/*
   Crie uma classe Numeros, que recebe um array de números racionais 
   (inteiros negativos, positivos ou com precisão numérica) informado
   na sua criação e que tenha um método calcularMediaSeguinte que 
   retorne um array com a média entre cada número e o número seguinte. 
*/

public class Numeros {

    final int tamanho = 5;
    double somaDeDois = 0;
    private double[] entrada= new double[tamanho];
    private double[] saida= new double[tamanho-1];
    
    public Numeros(double[] entrada){
      this.entrada=entrada;
    }
    //1,2,3,4,5
    public double[] calcularMediaSeguinte(){
      for(int i=1; i<tamanho;i++){
         somaDeDois = entrada[i-1] + entrada[i];
         saida[i-1] = somaDeDois/2;
      }
      return saida;
    }
}