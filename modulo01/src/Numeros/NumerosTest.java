
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class NumerosTest{
     @Test
     public void calcularMediaDeveReceberVetorERetornarMediaEntreElementos(){
         double[] entrada= {1.0,3.0,5.0,1.0,-10.0};
         double[] saidaEsperada= {2.0,4.0,3.0,-4.5};
         Numeros numerosRacionais = new Numeros(entrada);
         assertArrayEquals(saidaEsperada, numerosRacionais.calcularMediaSeguinte(), 0.1);
     }
     
     
}
