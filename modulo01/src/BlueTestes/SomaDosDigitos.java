import java.util.Scanner;
public class SomaDosDigitos{
    public static void main(String[] args){
        Scanner t = new Scanner(System.in);
        String numero = t.nextLine();
        byte divisor = t.nextByte();
        int digitosSomados = 0;
        char[] digitos = numero.toCharArray();
        for(int i = 0; i< digitos.length; i++){
            digitosSomados+=digitos[i];
        }
        System.out.println(testaDivisao(digitosSomados, (byte) divisor));
    }
    
    public static boolean testaDivisao(int digitosSomados, byte divisor){
        if((double)digitosSomados%divisor==0){
            return true;
        }
        return false;
    }
}
