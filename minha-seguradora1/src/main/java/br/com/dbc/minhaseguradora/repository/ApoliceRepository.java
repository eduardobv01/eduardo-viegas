package br.com.dbc.minhaseguradora.repository;

import br.com.dbc.minhaseguradora.entity.Apolice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApoliceRepository extends JpaRepository<Apolice, Long> {
    
}
