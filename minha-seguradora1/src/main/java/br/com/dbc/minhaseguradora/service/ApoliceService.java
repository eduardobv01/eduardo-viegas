package br.com.dbc.minhaseguradora.service;

import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.repository.ApoliceRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional( readOnly = true, rollbackFor = Exception.class )
public class ApoliceService {

    @Autowired
    private ApoliceRepository apoliceRepository;
    
    //save
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Apolice save( Apolice apolice ){
        return apoliceRepository.save(apolice);
    }
    
    //delete
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete( Long id ){
        apoliceRepository.deleteById(id);
    }
    
    //findById
    public Optional<Apolice> findById( Long id ){
        return apoliceRepository.findById(id);
    }

    //findAll
    public Page<Apolice> findAll( Pageable pa ){
        return apoliceRepository.findAll( pa );
    }
    
}
