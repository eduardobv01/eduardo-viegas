/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author hadaward
 */
@RestController
@RequestMapping("/api/midia")
public class MidiaRestController extends AbstractRestController<Midia>{

    @Autowired
    private MidiaService midiaService;
    
    @Override
    public AbstractService<Midia> getService() {
        return this.midiaService;
    }
    @GetMapping("/search")
    public ResponseEntity<?> search(Pageable pageable,
            @RequestParam(name = "categoria", required = false) Categoria categoria,
            @RequestParam(name = "titulo", required = false) String titulo,
            @RequestParam(name = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(name = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoFim) {

        return ResponseEntity.ok(this.midiaService.buscar(titulo, categoria, lancamentoIni, lancamentoFim, pageable));
    }
   
}
