package br.com.dbc.locadora1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Locadora1Application {

	public static void main(String[] args) {
		SpringApplication.run(Locadora1Application.class, args);
	}
}
