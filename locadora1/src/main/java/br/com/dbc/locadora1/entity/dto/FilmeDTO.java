/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity.dto;

import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@Builder
public class FilmeDTO {
    
    private Long id;
    private String titulo;
    private Categoria categoria;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "dd/MM/yyyy", shape=JsonFormat.Shape.STRING)
    private LocalDate lancamento;
    private List<MidiaDTO> midia;
    
    public Filme toFilme(){
        return Filme
                .builder()
                .titulo(this.titulo)
                .categoria(this.categoria)
                .lancamento(this.lancamento)
                .build();
        
    }
}
