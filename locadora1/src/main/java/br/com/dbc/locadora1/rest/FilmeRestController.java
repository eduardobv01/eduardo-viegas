/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import br.com.dbc.locadora1.entity.dto.FilmeDTO;
import br.com.dbc.locadora1.entity.dto.MidiaDTO;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.FilmeService;
import br.com.dbc.locadora1.service.MidiaService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author hadaward
 */
@RestController
@RequestMapping("/api/filme")
public class FilmeRestController extends AbstractRestController<Filme> {

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private MidiaService midiaService;

    @Override
    public AbstractService<Filme> getService() {
        return this.filmeService;
    }

    /* @PostMapping()
    public ResponseEntity<?> save(){
        //
    }*/
    @GetMapping("/search")
    public ResponseEntity<?> search(Pageable pageable,
            @RequestParam(name = "categoria", required = false) Categoria categoria,
            @RequestParam(name = "titulo", required = false) String titulo,
            @RequestParam(name = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoIni,
            @RequestParam(name = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lancamentoFim) {

        return ResponseEntity.ok(this.filmeService.buscar(titulo, categoria, lancamentoIni, lancamentoFim, pageable));
    }

    @GetMapping("/count/{idFilme}/{midiaType}")
    public ResponseEntity<?> getQuantidadePorMidia(@PathVariable Long idFilme, @PathVariable MidiaType midiaType) {
        Filme filme = filmeService.findById(idFilme).orElse(null);
        if (filme == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(this.filmeService.getQuantidadePorMidia(midiaType, filme));
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @PostMapping("/midia")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> save(@RequestBody FilmeDTO filmeDTO) {
        return ResponseEntity.ok(filmeService.save(filmeDTO));
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @PutMapping("/{id}/midia")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody FilmeDTO filmeDTO) {
        return ResponseEntity.ok(this.filmeService.update(id, filmeDTO));
    }

    @GetMapping("/{id}")
    @Override
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public ResponseEntity<?> get(@PathVariable Long id) {
        return this.getService().findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    
    @PostMapping("/search/catalogo")
    public ResponseEntity<?> getCatalogo(@RequestBody FilmeDTO filmeDTO) {
        return ResponseEntity.ok(this.filmeService.exibirCatalogo(filmeDTO.getTitulo()));
    }
    /*
    @GetMapping("/console/{id}")
    public ResponseEntity<?> getQuantidadePorMidia(@PathVariable Long id) {
        filmeService.exibirFilme(id);
        return ResponseEntity.ok(" ");
    }*/
}
