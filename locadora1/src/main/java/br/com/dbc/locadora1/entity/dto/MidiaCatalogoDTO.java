/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity.dto;

import br.com.dbc.locadora1.entity.MidiaType;
import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author hadaward
 */
@Data
@Builder
public class MidiaCatalogoDTO {
     private MidiaType tipo;
     private BigDecimal preco;
     private boolean disponivel;
     private LocalDate previsao;
}
