/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.ValorMidia;
import br.com.dbc.locadora1.repository.ClienteRepository;
import br.com.dbc.locadora1.repository.ValorMidiaRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class )
public class ValorMidiaService extends AbstractService<ValorMidia>{
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Override
    public JpaRepository<ValorMidia, Long> getRepository() {
        return this.valorMidiaRepository;
    }
    
    public List<ValorMidia> findByIdMidia(Midia idMidia){
        return this.valorMidiaRepository.findByIdMidia(idMidia);
    }
    public ValorMidia findByIdMidiaAndFinalVigenciaIsNull(Midia idMidia){
        return this.valorMidiaRepository.findByIdMidiaAndFinalVigenciaIsNull(idMidia);
    }
    
    public void deleteByIdMidia(Midia idMidia){
       this.valorMidiaRepository.deleteByIdMidia(idMidia);
    }
}
