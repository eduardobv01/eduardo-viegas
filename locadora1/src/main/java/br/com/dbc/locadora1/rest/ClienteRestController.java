/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;


import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.entity.dto.ClienteDTO;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author hadaward
 */

@RestController
@RequestMapping("/api/cliente")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class ClienteRestController extends AbstractRestController<Cliente> {
     
     @Autowired
     private ClienteService clienteService;
    
    @Override
    public AbstractService<Cliente> getService(){//Generic recebe ClienteService, que extende AbstractService
         return this.clienteService;
    }
    
    
    @PostMapping("/dto")
    public ResponseEntity<?> post(@RequestBody ClienteDTO input) {
        if (input.getId() != null) 
            return ResponseEntity.badRequest().build();
        System.out.println(input.getNome());
        return ResponseEntity.ok(this.clienteService.saveClienteDTO(input));
    }
 
      @GetMapping("/cep/{cep}")
      public ResponseEntity<?> post(@PathVariable String cep) {
        return ResponseEntity.ok(this.clienteService.consultaCEP(cep));
    }
     

}

