/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.repository;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hadaward
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    public int countByMidiaTypeAndIdFilme(MidiaType midiaType, Filme idFilme );
    
    public List<Midia> findByIdAluguel(Aluguel idAluguel);
    
    public List<Midia> findByIdFilme(Filme idFilme);
    
    public List<Midia> findByIdFilmeAndMidiaType(Filme idFilme, MidiaType midiaType);
    
    public Midia findFirstByIdFilmeAndMidiaTypeAndIdAluguelIsNull(Filme idFilme, MidiaType midiaType);
    
    public Page<Midia> findByIdFilmeTituloOrIdFilmeCategoriaOrIdFilmeLancamentoBetween(String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoFim, Pageable pageable);
   
}
