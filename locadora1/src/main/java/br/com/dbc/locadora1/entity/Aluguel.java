/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author eduardo.barbosa
 */
@Entity
@Table(name = "ALUGUEL")
@NamedQueries({
    @NamedQuery(name = "Aluguel.findAll", query = "SELECT a FROM Aluguel a")
    , @NamedQuery(name = "Aluguel.findById", query = "SELECT a FROM Aluguel a WHERE a.id = :id")
    , @NamedQuery(name = "Aluguel.findByRetirada", query = "SELECT a FROM Aluguel a WHERE a.retirada = :retirada")
    , @NamedQuery(name = "Aluguel.findByPrevisao", query = "SELECT a FROM Aluguel a WHERE a.previsao = :previsao")
    , @NamedQuery(name = "Aluguel.findByDevolucao", query = "SELECT a FROM Aluguel a WHERE a.devolucao = :devolucao")
    , @NamedQuery(name = "Aluguel.findByMulta", query = "SELECT a FROM Aluguel a WHERE a.multa = :multa")})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Aluguel extends AbstractEntity implements Serializable{
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//Utilizar no banco em memória
    @Column(name = "ID")
    private Long id;
    @Column(name = "RETIRADA")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate retirada;
    @Column(name = "PREVISAO")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate previsao;
    @Column(name = "DEVOLUCAO")
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime devolucao;
    @Column(name = "MULTA")
    private BigDecimal multa;
    @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID")
    @ManyToOne
    private Cliente idCliente;//Armazena null
}
