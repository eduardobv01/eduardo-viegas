/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import br.com.dbc.locadora1.entity.ValorMidia;
import br.com.dbc.locadora1.entity.dto.AluguelDTO;
import br.com.dbc.locadora1.entity.dto.FilmeCatalogoDTO;
import br.com.dbc.locadora1.entity.dto.FilmeDTO;
import br.com.dbc.locadora1.entity.dto.MidiaCatalogoDTO;
import br.com.dbc.locadora1.repository.AluguelRepository;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class AluguelService extends AbstractService<Aluguel> {

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;
    
    @Autowired
    private FilmeService filmeService;

    @Override
    public JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }

    public BigDecimal cacularMulta() {

        return new BigDecimal("15.5");
    }

    // private boolean isAlugada(){
    //return true;     
    //}
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void alugar(AluguelDTO aluguelDTO) {
        Optional<Cliente> c = this.clienteService.findById(aluguelDTO.getIdCliente());
        List<ValorMidia> listaValorMidia;
        if (c.isPresent()) {
            System.out.println(c.get().getNome());
            Aluguel aluguel = Aluguel.builder()
                    .retirada(LocalDate.now())
                    .previsao(LocalDate.now().plusDays(aluguelDTO.getMidias().size()))
                    .multa(new BigDecimal("0.0"))
                    .idCliente(c.get())
                    .build();
            this.aluguelRepository.save(aluguel);
            BigDecimal preco = new BigDecimal("0.0");
            aluguelDTO.getMidias().forEach(idMidia -> {
                Midia midia = midiaService.findById(idMidia).get();

                midia.setIdAluguel(aluguel);
                midiaService.save(midia);
                System.out.println(midiaService.findById(idMidia).get().getIdAluguel().getId()
                        + midiaService.findById(idMidia).get().getMidiaType().toString());

            });
            
            ValorMidia.builder().build().setValor(preco);
            this.aluguelRepository.save(aluguel);
        }
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void devolver(List<Long> midias) {
        Long idAluguel = midiaService.findById(midias.get(0)).get().getIdAluguel().getId();
        Aluguel aluguel = this.aluguelRepository.findById(idAluguel).get();
        List<Midia> listaMidias = this.midiaService.findByIdAluguel(aluguel), midiasDevolvidas = new ArrayList<>();
        midias.forEach(idMidia ->{
            listaMidias.forEach(midia->{
                if (midia.getId().equals(idMidia)) 
                    midiasDevolvidas.add(midia);});
            Midia midia = midiaService.findById(idMidia).get();
            midia.setIdAluguel(null);
            midiaService.save(midia);
        });
       BigDecimal multa = BigDecimal.ZERO, preco = this.calcularPreco(midiasDevolvidas);
       if (midias.isEmpty()) {
            aluguel.setDevolucao(LocalDateTime.now());
            boolean atrasouHorario = LocalDateTime.now().getHour() > 16, atrasouData = LocalDateTime.now().toLocalDate().isAfter(aluguel.getPrevisao());
            if (atrasouData||atrasouHorario) {
                multa = aluguel.getMulta().add(preco.multiply(new BigDecimal("2")));
            }
        }
        aluguel.setMulta(multa);
        this.aluguelRepository.save(aluguel);
    }

    private BigDecimal calcularPreco(List<Midia> midias) {
        BigDecimal precoTotal = BigDecimal.ZERO;
        midias.forEach(midia-> {
               List<ValorMidia> preco = this.valorMidiaService.findByIdMidia(midia);
               precoTotal.add(preco.get(preco.size() - 1).getValor());
        });
        return precoTotal;
    }

    public List<Midia> getMidiaList(Aluguel idAluguel) {
        return midiaService.findByIdAluguel(idAluguel);
    }

    /*public /*List<Aluguel>void getFilmesDevolverHoje(){
        List<Aluguel> alugueis = this.aluguelRepository.findByPrevisao(LocalDate.now());
        List<Filme> filmes = new ArrayList();
        final List<Midia> midias;
            midias = this.midiaService.findByIdAluguel(aluguel);
           
            
           filmes.add(e)
        });*/
    //}
    /**/
    
    /*
    List<Midia> novasMidias = new ArrayList();
    midias.forEach(midia -> {
                 Midia midiaDevolvida = midiaService.findById(midia).get();
                 Aluguel aluguel = midiaDevolvida.getIdAluguel();
                 aluguel.setDevolucao(LocalDateTime.now());
                 List<ValorMidia> valorMidia= this.valorMidiaService.findByIdMidia(midiaDevolvida);
                 ValorMidia ultimoValor = valorMidia.get(valorMidia.size()-1);
                 if(aluguel.getDevolucao().toLocalDate().isAfter(aluguel.getPrevisao())){
                        aluguel.getMulta().add(ultimoValor.getValor().multiply(new BigDecimal("2")));
                 }else{
                        aluguel.setMulta(BigDecimal.ZERO);
                 }
                 midias.forEach(elementoMidia->{
                         Midia midiaAtual = midiaService.findById(0l).get();
                         midiaAtual.setIdAluguel(null);
                         midiaService.save(midiaAtual);
                 });
                 this.aluguelRepository.save(aluguel);
                 novasMidias.add(midiaDevolvida);
            });
         return midiaService.findById(midias.get(0)).get().getIdAluguel();
       */
}
