/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.repository;

import br.com.dbc.locadora1.entity.Aluguel;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hadaward
 */
public interface AluguelRepository extends JpaRepository<Aluguel, Long> {
    public List<Aluguel> findByPrevisao(LocalDate previsao);
}
