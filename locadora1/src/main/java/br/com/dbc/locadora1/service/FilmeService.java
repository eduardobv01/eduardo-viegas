/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import br.com.dbc.locadora1.entity.ValorMidia;
import br.com.dbc.locadora1.entity.dto.FilmeCatalogoDTO;
import br.com.dbc.locadora1.entity.dto.FilmeDTO;
import br.com.dbc.locadora1.entity.dto.MidiaCatalogoDTO;
import br.com.dbc.locadora1.repository.FilmeRepository;
import br.com.dbc.locadora1.repository.MidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FilmeService extends AbstractService<Filme> {

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Autowired
    private FilmeService filmeService;

    @Override
    public JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }

    public Page<Filme> buscar(String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoFim, Pageable pageable) {
        return this.filmeRepository.findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(titulo, categoria, lancamentoIni, lancamentoFim, pageable); //
    }

    public int getQuantidadePorMidia(MidiaType midiaType, Filme filme) {
        return this.midiaRepository.countByMidiaTypeAndIdFilme(midiaType, filme);
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme save(FilmeDTO filmeDTO) {
        Filme filme = this.filmeRepository.save(filmeDTO.toFilme());
        filmeDTO.getMidia().forEach(midia -> {
            midiaService.save(midia, filme);
        });
        return filme;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme update(Long id, FilmeDTO filmeDTO) {

        Filme filme = filmeRepository.save(Filme.builder()
                .id(id)
                .titulo(filmeDTO.getTitulo())
                .lancamento(filmeDTO.getLancamento())
                .categoria(filmeDTO.getCategoria())
                .build());

        filmeDTO.getMidia().forEach(midia -> {
            List<Midia> midias = midiaService.findByIdFilmeAndMidiaType(filme, midia.getTipo());
            if (midia.getQuantidade() <= midias.size()) {
                if (midias.size() != midia.getQuantidade()) {
                    for (int i = 0; i < midias.size() - midia.getQuantidade(); i++) {
                        midias.get(i).setIdAluguel(null);
                        valorMidiaService.deleteByIdMidia(midias.get(i));
                        midiaService.delete(midias.get(i).getId());
                    }
                }
            } else {
                midia.setQuantidade(midia.getQuantidade() - midias.size());
                for (int i = 0; i < midia.getQuantidade(); i++) {
                    Midia novaMidia = Midia.builder()
                            .midiaType(midia.getTipo())
                            .idAluguel(null)
                            .idFilme(filme)
                            .build();
                    midiaRepository.save(novaMidia);

                    ValorMidia novoValor = ValorMidia.builder()
                            .valor(midia.getValor())
                            .inicioVigencia(LocalDateTime.now())
                            .finalVigencia(null)
                            .idMidia(novaMidia)
                            .build();

                    valorMidiaService.save(novoValor);
                }
            }
            midias = midiaService.findByIdFilmeAndMidiaType(filme, midia.getTipo());
            midias.forEach((unidMidia) -> {
                List<ValorMidia> valoresMidia = valorMidiaService.findByIdMidia(unidMidia);
                ValorMidia valorAtual = valoresMidia.get(valoresMidia.size() - 1);
                if (valorAtual.getValor().compareTo(midia.getValor()) != 0) {
                    valorAtual.setFinalVigencia(LocalDateTime.now());
                    ValorMidia novoValor = ValorMidia.builder()
                            .valor(midia.getValor())
                            .inicioVigencia(LocalDateTime.now())
                            .finalVigencia(null)
                            .idMidia(unidMidia)
                            .build();
                    valorMidiaService.save(novoValor);
                }
            });
        });
        return filme;
    }

    public List<FilmeCatalogoDTO> exibirCatalogo(String titulo) {
        List<FilmeCatalogoDTO> filmesCatalogo = new ArrayList();

        List<Filme> filmes = this.filmeService.findByTituloIgnoreCaseContaining(titulo);
        filmes.forEach(filme -> {
            FilmeCatalogoDTO filmeCatalogo = FilmeCatalogoDTO.builder()
                    .titulo(filme.getTitulo())
                    .categoria(filme.getCategoria())
                    .lancamento(filme.getLancamento())
                    .build();

            List<MidiaCatalogoDTO> midias = new ArrayList();

            MidiaType[] tipos = MidiaType.values();

            for (MidiaType tipo : tipos) {
                MidiaCatalogoDTO midiaCatalogo = MidiaCatalogoDTO.builder().build();
                Midia midiaDisponivel = this.midiaService.findFirstByIdFilmeAndMidiaTypeAndIdAluguelIsNull(filme, tipo);
                boolean disponivel = midiaDisponivel != null;
                midiaCatalogo.setTipo(tipo);
                midiaCatalogo.setDisponivel(disponivel);
                List<Midia> midiasIndisponiveis;
                if (disponivel) {
                    ValorMidia valorVigente = this.valorMidiaService.findByIdMidiaAndFinalVigenciaIsNull(midiaDisponivel);
                    midiaCatalogo.setPreco(valorVigente.getValor());
                } else {
                    midiasIndisponiveis = this.midiaService.findByIdFilmeAndMidiaType(filme, tipo);
                    LocalDate dataMaisProxima = LocalDate.MAX;
                    for (Midia midiaIndisponivel : midiasIndisponiveis) {
                        if (midiaIndisponivel.getIdAluguel().getPrevisao().compareTo(dataMaisProxima) > 0) {
                            dataMaisProxima = midiaIndisponivel.getIdAluguel().getPrevisao();
                        }
                    }
                    midiaCatalogo.setPrevisao(dataMaisProxima);
                }
                midias.add(midiaCatalogo);
            }

            filmeCatalogo.setMidias(midias);
            filmesCatalogo.add(filmeCatalogo);
        });
        return filmesCatalogo;
    }

    public List<Filme> findByTituloIgnoreCaseContaining(String titulo) {
        return this.filmeRepository.findByTituloIgnoreCaseContaining(titulo);
    }

    /*
    public void exibirFilme(Long id) {
        System.out.println("br.com.dbc.locadora1.service.FilmeService.exibirFilme()");
        Filme filme = this.filmeRepository.findById(id).get();
        if(filme!=null){
        System.out.println(filme.getTitulo());
        System.out.println(filme.getLancamento());
        System.out.println(filme.getCategoria());
        }else{
            System.out.println("Filme nulo");
        }
    }*/
}
