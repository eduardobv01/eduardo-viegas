/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity.dto;

import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.MidiaType;
import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author hadaward
 */
@Data
@Builder
public class FilmeCatalogoDTO {
      private String titulo;
      private Categoria categoria;
      private LocalDate lancamento;
      private List<MidiaCatalogoDTO> midias;
}
