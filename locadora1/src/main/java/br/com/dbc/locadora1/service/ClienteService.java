/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import br.com.dbc.locadora1.config.SoapConnector;
import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.entity.dto.ClienteDTO;
import br.com.dbc.locadora1.entity.dto.EnderecoDTO;
import br.com.dbc.locadora1.repository.ClienteRepository;
import br.com.dbc.locadora1.ws.ConsultaCEP;
import br.com.dbc.locadora1.ws.ConsultaCEPResponse;
import br.com.dbc.locadora1.ws.ObjectFactory;
import java.util.List;
import java.util.Optional;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class )
public class ClienteService extends AbstractService<Cliente>{
    
    private String url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired 
    private ObjectFactory objectFactory;

    @Autowired 
    private SoapConnector soapConnector;
    
    @Override
    public JpaRepository<Cliente, Long> getRepository() {
        return clienteRepository;
    }
     @Transactional(readOnly = false, rollbackFor = Exception.class )
     public Cliente saveClienteDTO(ClienteDTO clienteDTO){
          Cliente cliente = Cliente
                  .builder()
                  .nome(clienteDTO.getNome())
                  .endereco("Rua " + clienteDTO.getRua() + ", " +clienteDTO.getNumero() + 
                          ". " + clienteDTO.getCidade() + "/" + clienteDTO.getEstado())
                  .telefone(clienteDTO.getTelefone())
                  .build();
        return this.clienteRepository.save(cliente);
    }
    
    public EnderecoDTO consultaCEP(String cep){
        ConsultaCEP consulta = objectFactory.createConsultaCEP();
        consulta.setCep(cep);
        JAXBElement<ConsultaCEPResponse> resposta = (JAXBElement<ConsultaCEPResponse>) soapConnector.callWebService(url, objectFactory.createConsultaCEP(consulta));
        EnderecoDTO endereco = EnderecoDTO.builder()
                .rua(resposta.getValue().getReturn().getEnd())
                .bairro(resposta.getValue().getReturn().getBairro())
                .cidade(resposta.getValue().getReturn().getCidade())
                .estado(resposta.getValue().getReturn().getUf())
                .build();
       
        return endereco; 
    }
    
    
}
