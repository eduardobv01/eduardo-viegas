/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.repository;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 *
 * @author hadaward
 */
public interface FilmeRepository extends JpaRepository<Filme, Long> {
        
    public Filme findByCategoria(Categoria categoria);
    
    public List<Filme> findByTituloIgnoreCaseContaining(String titulo);
    
    public Page<Filme> findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(String titulo, Categoria categoria, LocalDate dataInicial, LocalDate dataFinal, Pageable pageable);
    
}
