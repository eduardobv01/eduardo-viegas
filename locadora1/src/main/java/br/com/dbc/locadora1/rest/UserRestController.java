/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.domain.User;
import br.com.dbc.locadora1.entity.dto.FilmeDTO;
import br.com.dbc.locadora1.entity.dto.SenhaDTO;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.impl.AppUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author eduardo.barbosa
 */
@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
public class UserRestController extends AbstractRestController<User>{
     
    @Autowired
    private AppUserDetailsService userService;
    
    public AbstractService<User> getService(){//Generic recebe ClienteService, que extende AbstractService
         return this.userService;
    }
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @PostMapping("/password")//PutMapping
    public ResponseEntity<?> changePassword(@RequestBody SenhaDTO senha) {
        System.out.println(senha.getUsername() + ": " + senha.getPassword());
        if(senha == null)
             return ResponseEntity.notFound().build();
        return ResponseEntity.ok(this.userService.changePassword(senha.getUsername(), senha.getPassword()));
    }
    
    
}
