/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author eduardo.barbosa
 */
@Entity
@Table(name = "FILME")
@NamedQueries({
    @NamedQuery(name = "Filme.findAll", query = "SELECT f FROM Filme f")
    , @NamedQuery(name = "Filme.findById", query = "SELECT f FROM Filme f WHERE f.id = :id")
    , @NamedQuery(name = "Filme.findByTitulo", query = "SELECT f FROM Filme f WHERE f.titulo = :titulo")
    , @NamedQuery(name = "Filme.findByCategoria", query = "SELECT f FROM Filme f WHERE f.categoria = :categoria")
    , @NamedQuery(name = "Filme.findByLancamento", query = "SELECT f FROM Filme f WHERE f.lancamento = :lancamento")})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Filme extends AbstractEntity implements Serializable{
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//Utilizar no banco em memória
    @Column(name = "ID")
    private Long id;
    @Size(max = 100)
    @Column(name = "TITULO")
    private String titulo;
    @Column(name = "CATEGORIA")
    @Enumerated(value = EnumType.STRING)
    private Categoria categoria;
    @Column(name = "LANCAMENTO")
    @DateTimeFormat(pattern = "dd/MM/yyyy")//Alterado dd-MM-yyy para dd/MM/yyyy
    @JsonFormat(pattern = "dd/MM/yyyy", shape=JsonFormat.Shape.STRING)
    private LocalDate lancamento;
}
