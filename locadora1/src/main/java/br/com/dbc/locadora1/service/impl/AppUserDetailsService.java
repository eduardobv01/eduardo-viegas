package br.com.dbc.locadora1.service.impl;

import br.com.dbc.locadora1.domain.User;
import br.com.dbc.locadora1.repository.UserRepository;
import br.com.dbc.locadora1.rest.AbstractRestController;
import br.com.dbc.locadora1.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by nydiarra on 06/05/17.
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class AppUserDetailsService extends AbstractService<User> implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public JpaRepository<User, Long> getRepository() {
        return this.userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        });

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);

        return userDetails;
    }
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public User changePassword(String username, String password) {
        System.out.println(username + ": " + password);
        User user = this.userRepository.findByUsername(username);
        if (user.getPassword() != password) {
            user.setPassword(password);
            this.userRepository.save(user);
        }
        return user;
    }
}
