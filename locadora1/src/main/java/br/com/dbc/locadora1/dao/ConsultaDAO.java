/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

package br.com.dbc.locadora1.dao;

import br.com.dbc.locadora1.config.SoapConnector;
import br.com.dbc.locadora1.entity.dto.EnderecoDTO;
import br.com.dbc.locadora1.ws.ConsultaCEP;
import br.com.dbc.locadora1.ws.ConsultaCEPResponse;
import br.com.dbc.locadora1.ws.ObjectFactory;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.JAXBElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hadaward
 
@Data
public class ConsultaDAO {

    private String cep;
    private final String url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";

    public ConsultaDAO(String cep) {
        this.cep = cep;
    }

    @Autowired
    private ObjectFactory objectFactory;

    @Autowired
    private SoapConnector soapConnector;

    public EnderecoDTO consultarCEP() {
        ConsultaCEP consulta = objectFactory.createConsultaCEP();
        consulta.setCep(cep);
        JAXBElement<ConsultaCEPResponse> resposta = (JAXBElement<ConsultaCEPResponse>) soapConnector.callWebService(url, objectFactory.createConsultaCEP(consulta));
        return EnderecoDTO.builder()
                .rua(resposta.getValue().getReturn().getEnd())
                .bairro(resposta.getValue().getReturn().getBairro())
                .cidade(resposta.getValue().getReturn().getCidade())
                .estado(resposta.getValue().getReturn().getUf())
                .build();
    }

}*/
