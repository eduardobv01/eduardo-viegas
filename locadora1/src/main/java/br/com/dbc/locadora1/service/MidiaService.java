/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.service;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import br.com.dbc.locadora1.entity.ValorMidia;
import br.com.dbc.locadora1.entity.dto.MidiaDTO;
import br.com.dbc.locadora1.repository.MidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hadaward
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class MidiaService extends AbstractService<Midia> {

    @Autowired
    private MidiaRepository midiaRepository;
    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    public JpaRepository<Midia, Long> getRepository() {
        return this.midiaRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void save(MidiaDTO midiaDTO, Filme filme) {//Sobrecarga
        for (int i = 0; i < midiaDTO.getQuantidade(); i++) {//Armazena os registros conforme a quantidade de midias a ser salvas
            Midia midia = midiaRepository.save(midiaDTO.toMidia(filme));
            valorMidiaService.save(ValorMidia.builder()
                    .idMidia(midia)
                    .inicioVigencia(LocalDateTime.now())
                    .valor(midiaDTO.getValor())
                    .build());
        }
    }

    public Page<Midia> buscar(String titulo, Categoria categoria, LocalDate lancamentoIni, LocalDate lancamentoFim, Pageable pageable) {
        return this.midiaRepository.findByIdFilmeTituloOrIdFilmeCategoriaOrIdFilmeLancamentoBetween(titulo, categoria, lancamentoIni, lancamentoFim, pageable);
    }

    public void deleteByIdMidia(Long idMidia) {
        this.midiaRepository.deleteById(idMidia);

    }

    public List<Midia> findByIdAluguel(Aluguel idAluguel) {
        return this.midiaRepository.findByIdAluguel(idAluguel);
    }

    public List<Midia> findByIdFilme(Filme idFilme) {
        return this.midiaRepository.findByIdFilme(idFilme);
    }

    public List<Midia> findByIdFilmeAndMidiaType(Filme idFilme, MidiaType midiaType) {
        return this.midiaRepository.findByIdFilmeAndMidiaType(idFilme, midiaType);
    }

    public Midia findFirstByIdFilmeAndMidiaTypeAndIdAluguelIsNull(Filme idFilme, MidiaType midiaType) {
        return this.midiaRepository.findFirstByIdFilmeAndMidiaTypeAndIdAluguelIsNull(idFilme, midiaType);
    }

    public Midia findByIdFilmeAndMidiaTypeAndIdAluguel(Filme idFilme, MidiaType midiaType) {
        return this.midiaRepository.findFirstByIdFilmeAndMidiaTypeAndIdAluguelIsNull(idFilme, midiaType);
    }

}
