/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.entity.dto;

import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author hadaward
 */
@Data
@Builder
public class MidiaDTO {

    private MidiaType tipo;
    private int quantidade;
    private BigDecimal valor;
    private Filme idFilme;

    public Midia toMidia(Filme filme) {
        return Midia
                .builder()
                .midiaType(this.tipo)
                .idAluguel(null)
                .idFilme(filme)
                .build();
    }
}
