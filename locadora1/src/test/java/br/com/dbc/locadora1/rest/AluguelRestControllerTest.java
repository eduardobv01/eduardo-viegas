/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.Locadora1ApplicationTests;
import br.com.dbc.locadora1.entity.Aluguel;
import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import br.com.dbc.locadora1.entity.dto.AluguelDTO;
import br.com.dbc.locadora1.entity.dto.FilmeDTO;
import br.com.dbc.locadora1.entity.dto.MidiaDTO;
import br.com.dbc.locadora1.repository.AluguelRepository;
import br.com.dbc.locadora1.repository.ClienteRepository;
import br.com.dbc.locadora1.repository.MidiaRepository;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.AluguelService;
import br.com.dbc.locadora1.service.ClienteService;
import br.com.dbc.locadora1.service.MidiaService;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author hadaward
 */
public class AluguelRestControllerTest extends Locadora1ApplicationTests{
    
    @Autowired
    private ClienteService clienteService;
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired
    private AluguelService aluguelService;
    
    @Autowired
    private AluguelRepository aluguelRepository;
     
    @Autowired
    private AluguelRestController aluguelRestController;
    
    public AluguelRestControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void beforeTests() {
        clienteRepository.deleteAll();
        midiaRepository.deleteAll();
        aluguelRepository.deleteAll();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of retirada method, of class AluguelRestController.
     */
    @Ignore
    @Test
    public void testRetirada() throws Exception{
        Cliente cliente = clienteService.save(Cliente.builder().nome("João").endereco("Teste").telefone("5199999999").build());
        Filme filme = Filme.builder().titulo("Up").categoria(Categoria.ANIMACAO).lancamento(LocalDate.now()).build();
        ArrayList<Long> idMidias = new ArrayList();
        idMidias.addAll(Arrays.asList(
        this.midiaRepository.save(Midia.builder().midiaType(MidiaType.DVD).idFilme(filme).build()).getId(),
        this.midiaRepository.save(Midia.builder().midiaType(MidiaType.VHS).idFilme(filme).build()).getId(),
        this.midiaRepository.save(Midia.builder().midiaType(MidiaType.BLUE_RAY).idFilme(filme).build()).getId()
        ));
        AluguelDTO aluguelDTO = AluguelDTO.builder().idCliente(cliente.getId()).midias(idMidias).build();
        this.aluguelService.alugar(aluguelDTO);
        //restMockMvc.perform(MockMvcRequestBuilders.put("/api/aluguel/devolucao")
               // .contentType(MediaType.APPLICATION_JSON_UTF8)
               // .content(objectMapper.writeValueAsBytes(idMidias)))
               // .andExpect(MockMvcResultMatchers.status().isOk())
                //.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                //.andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                //.andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(novoFilme.getTitulo()))
                //.andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(novoFilme.getCategoria().toString()))
                //.andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(novoFilme.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
    }

    /**
     * Test of devolver method, of class AluguelRestController.
     */
    @Ignore
    @Test
    public void testDevolver() {
        System.out.println("devolver");
        AluguelDTO alugueDTO = null;
        AluguelRestController instance = new AluguelRestController();
        ResponseEntity expResult = null;
        ResponseEntity result = instance.devolver(alugueDTO);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Override
    protected AbstractRestController<Aluguel> getController() {
        return this.aluguelRestController;
    }
    
}
