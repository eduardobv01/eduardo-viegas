/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.Locadora1ApplicationTests;
import br.com.dbc.locadora1.rest.AbstractRestController;
import br.com.dbc.locadora1.rest.FilmeRestController;
import br.com.dbc.locadora1.Locadora1ApplicationTests;
import br.com.dbc.locadora1.entity.Categoria;
import br.com.dbc.locadora1.entity.Filme;
import br.com.dbc.locadora1.entity.Midia;
import br.com.dbc.locadora1.entity.MidiaType;
import br.com.dbc.locadora1.entity.dto.FilmeDTO;
import br.com.dbc.locadora1.entity.dto.MidiaDTO;
import br.com.dbc.locadora1.repository.ClienteRepository;
import br.com.dbc.locadora1.repository.FilmeRepository;
import br.com.dbc.locadora1.repository.MidiaRepository;
import br.com.dbc.locadora1.repository.ValorMidiaRepository;
import br.com.dbc.locadora1.service.AluguelService;
import br.com.dbc.locadora1.service.ClienteService;
import br.com.dbc.locadora1.service.FilmeService;
import br.com.dbc.locadora1.service.MidiaService;
import java.math.BigDecimal;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author hadaward
 */
public class FilmeRestControllerTest extends Locadora1ApplicationTests {

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private FilmeService filmeService;

  
    protected AbstractRestController<Filme> getController() {
        return filmeRestController;
    }

    @Before
    public void beforeTest() {
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();;
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

   
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testFilmeCreate() throws Exception {
 
        FilmeDTO filme =  criarExemplarFilmeDTO("João e o Feijão", Categoria.AVENTURA);

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filme.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        assertEquals(1, filmes.size());
        assertEquals(filme.getTitulo(), filmes.get(0).getTitulo());
        assertEquals(filme.getLancamento(), filmes.get(0).getLancamento());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testFilmeUpdate() throws Exception {
        
        Filme filme =this.filmeService.save(criarExemplarFilmeDTO("João e o Feijão", Categoria.AVENTURA));
        FilmeDTO novoFilme = criarExemplarFilmeDTO("João e o Feijão1", Categoria.ACAO);
        novoFilme.setId(filme.getId());
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", filme.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(novoFilme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(novoFilme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(novoFilme.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(novoFilme.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        assertEquals(1, filmes.size());
        assertEquals(novoFilme.getTitulo(), filmes.get(0).getTitulo());
        assertEquals(novoFilme.getLancamento(), filmes.get(0).getLancamento());
    }
    
    private FilmeDTO criarExemplarFilmeDTO(String titulo, Categoria categoria){
        List<MidiaDTO> midias = Arrays.asList(
                MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(10).valor(new BigDecimal("1.9")).build(), 
                MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(10).valor(new BigDecimal("2.9")).build(),
                MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(10).valor(new BigDecimal("3.9")).build());
        return FilmeDTO.builder()
                .titulo(titulo)
                .categoria(categoria)
                .lancamento(LocalDate.parse("08/08/2016", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .midia(midias)
                .build();
    }
     private FilmeDTO criarExemplarFilmeDTO(String titulo,  List<MidiaDTO> midias){
        return FilmeDTO.builder()
                .titulo(titulo)
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.parse("08/08/2016", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .midia(midias)
                .build();
    }


    

}
       