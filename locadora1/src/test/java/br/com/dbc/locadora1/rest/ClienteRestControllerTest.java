/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.Locadora1ApplicationTests;
import br.com.dbc.locadora1.entity.Cliente;
import br.com.dbc.locadora1.entity.dto.ClienteDTO;
import br.com.dbc.locadora1.repository.ClienteRepository;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.ClienteService;
import java.util.List;
import static org.bouncycastle.asn1.x500.style.RFC4519Style.c;
import org.h2.mvstore.Page;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author hadaward
 */
public class ClienteRestControllerTest extends Locadora1ApplicationTests{
    
    @Autowired
    private ClienteRepository clienteRepository;
   
    @Autowired
    private ClienteService clienteService;
    
    @Autowired
    private ClienteRestController clienteRestController;
  
    
    public ClienteRestControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
   
    @Before
    public void before() {
        clienteRepository.deleteAll();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getService method, of class ClienteRestController.
     */
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void testGetUsers() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/springjwt/users"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
               //      .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }
    
    /*@Test(expected = Exception.class)
    @WithMockUser(username="john.doe", 
            password = "jwtpass", 
            authorities = {"STANDARD_USER"})
    public void testGetUsersAccessDenied() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/springjwt/users"));
    }*/
    
    @Test
    @WithMockUser(username="admin.admin", 
            password = "jwtpass", 
            authorities = {"ADMIN_USER"})
    public void testpostCliente() throws Exception {
        ClienteDTO clienteDTO = null;
        clienteDTO = ClienteDTO.builder()
                .nome("João")
                .telefone("5199999999")
                .numero("531")
                .rua("Avenida Pátria")
                .bairro("São Geraldo")
                .cidade("Porto Alegre")
                .estado("RS")
                .build();
        
        String clienteEndereco = "Rua " + clienteDTO.getRua() + ", " +clienteDTO.getNumero() + ". " + clienteDTO.getCidade() + "/" + clienteDTO.getEstado();
        //mocando conexão com cliente
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente/dto")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(clienteDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())//Resposta mocada
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(clienteDTO.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(clienteDTO.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endereco").value(clienteEndereco));
        List<Cliente> clientes = this.clienteRepository.findAll();
        assertEquals(clienteDTO.getNome(), clientes.get(0).getNome());
        assertEquals(1, clientes.size());
        assertEquals(clienteDTO.getTelefone(), clientes.get(0).getTelefone());
        assertTrue(clientes.get(0).getEndereco().contains(clienteDTO.getRua()));
        assertFalse(clientes.get(0).getEndereco().contains(clienteDTO.getBairro()));
        assertTrue(clientes.get(0).getEndereco().contains(clienteDTO.getNumero()));
        assertTrue(clientes.get(0).getEndereco().contains(clienteDTO.getEstado()));
        
    }

    /**
     * Test of post method, of class ClienteRestController.
     */
   /* @Test
    public void testPost_ClienteDTO() {
        System.out.println("post");
        ClienteDTO input = null;
        ClienteRestController instance = new ClienteRestController();
        ResponseEntity expResult = null;
        ResponseEntity result = instance.post(input);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    
    @Test
    public void testPost_String() {
        System.out.println("post");
        String cep = "";
        ClienteRestController instance = new ClienteRestController();
        ResponseEntity expResult = null;
        ResponseEntity result = instance.post(cep);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    @Override
    protected ClienteRestController getController() {
        return this.clienteRestController;
    }
    
}
