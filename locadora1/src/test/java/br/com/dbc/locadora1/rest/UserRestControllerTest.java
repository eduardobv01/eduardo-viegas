/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora1.rest;

import br.com.dbc.locadora1.Locadora1ApplicationTests;
import br.com.dbc.locadora1.domain.User;
import br.com.dbc.locadora1.entity.dto.SenhaDTO;
import br.com.dbc.locadora1.repository.FilmeRepository;
import br.com.dbc.locadora1.repository.UserRepository;
import br.com.dbc.locadora1.service.AbstractService;
import br.com.dbc.locadora1.service.impl.AppUserDetailsService;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author hadaward
 */
public class UserRestControllerTest extends Locadora1ApplicationTests{
    
    @Autowired
    private UserRestController userRestController;
    
    @Autowired
    private AppUserDetailsService appUserDetailsService;
    
    @Autowired
    private UserRepository userRepository;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void beforeTest() {
        userRepository.deleteAll();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of changePassword method, of class UserRestController.
     */
    
    @Test
     @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testChangePassword() throws Exception {
        
        User usuario = this.appUserDetailsService.save(User.builder()
                .firstName("João")
                .lastName("Doe")
                .username("joaodoe")
                .password("123joao").build());
        
        SenhaDTO novaSenha = SenhaDTO.builder()
                .username("joaodoe")
                .password("321joao")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user/password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(novaSenha)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(usuario.getUsername()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").value(novaSenha.getPassword()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(usuario.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(usuario.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roles").isEmpty());
        
                 List<User> usuarios = this.userRepository.findAll();
                 assertEquals(novaSenha.getPassword(), usuarios.get(0).getPassword());
                 assertEquals(novaSenha.getUsername(), usuarios.get(0).getUsername());
    }
    
    @Ignore
    @Test
    public void testGetService() {
        System.out.println("getService");
        UserRestController instance = new UserRestController();
        AbstractService<User> expResult = null;
        AbstractService<User> result = instance.getService();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Override
    protected AbstractRestController<User> getController() {
        return this.userRestController; 
    }
    
}
